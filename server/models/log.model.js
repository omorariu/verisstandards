'use strict';


var mongoose = require('mongoose');

var logSchema = new mongoose.Schema({
	id : String,
	time: String,
	message: String
});



module.exports = mongoose.model('Log', logSchema);