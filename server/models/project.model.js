'use strict';


var mongoose = require('mongoose');

var projectSchema = new mongoose.Schema({
    projectName: {type: String, required: true, unique: true},
    projectDesc: {type: String, required: false},
    alchemyCustomModel: {type: String, required: false},
    status: {type:String, required:false},
    solrClusterStd: {type:String, required:false},
    solrClusterComp: {type:String, required:false},
    rankerStd: {type:String, required:false},
    rankerComp: {type:String, required:false},
    solrClusterStandards: {type:String, required:false},
    solrClusterCompany: {type:String, required:false},
    
    standardFiles: [{
        name : String,
        path : String
         }],
    standardIBlocks: [{
             ibid : String,
             orgiginal_path : String,
             text : String,
             keywords : [
                     {
                    	 keyword : String,
                    	 relevance : Number
                     }
             ],
             cross_score : [
                     {
                    	 fibid: String,
                    	 score: Number
                     }
             ]
    
     }],
     companyFiles: [{
        name : String,
        path : String
         }],
     companyIBlocks: [{
             ibid : String,
             orgiginal_path : String,
             text : String,
             keywords : [
                     {
                    	 keyword : String,
                    	 relevance : Number
                     }
             ],
             cross_score : [
                     {
                    	 fibid: String,
                    	 score: Number
                     }
             ]

      }]
      
},{ versionKey: false });


module.exports = mongoose.model('Project', projectSchema);
