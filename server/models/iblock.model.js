'use strict';


var mongoose = require('mongoose');

var iblockSchema = new mongoose.Schema({
	
	id: {type:String, required:true},
    doc : {type:String, required:true},
    projectName : {type:String, required:true},
    doc_type : {type:String, required:true},
	type: {type:String, required:false},
    parent_id: {type:String, required:false},
    title: {type:String, required:false},
    direction: {type:String, required:false},
    content: [
      {
        media_type: {type:String, required:false},
        text: {type:String, required:false}
      }
    ],
    keywords: [
               {
		    	relevance: {type: Number, required: false},
		    	text: {type:String, required:false}
               }          
    ]
 
});


module.exports = mongoose.model('IBlock', iblockSchema);
