'use strict';


var mongoose = require('mongoose');

var matchedReportItem = new mongoose.Schema({
	
	id: {type:String, required:true},
    doc : {type:String, required:true},
    projectName : {type:String, required:true},
    doc_type : {type:String, required:true},
	type: {type:String, required:false},
    parent_id: {type:String, required:false},
    title: {type:String, required:false},
    direction: {type:String, required:false},
    txt: {type:String, required:false},
    keywords: {type:String, required:false},
    score: {type:Number, required:false},
    match: {type:String, required:false},
    matchedDocID :{type:String, required:false},
    matchedDocTitle :{type:String, required:false},
    matchedDocTxt : {type:String, required:false},
    matchedDocScore : {type:Number, required:false},
    
    matchedDocKeywords : {type:String, required:false},
    matchedDocSourceFile : {type:String, required:false},
    matchedDocID2 :{type:String, required:false},
    matchedDocTitle2 :{type:String, required:false},
    matchedDocTxt2 : {type:String, required:false},
    matchedDocKeywords2 : {type:String, required:false},
    matchedDocSourceFile2 : {type:String, required:false},
    matchedDocScore2 : {type:Number, required:false},
    
    matchedDocID3 :{type:String, required:false},
    matchedDocTitle3 :{type:String, required:false},
    matchedDocTxt3 : {type:String, required:false},
    matchedDocKeywords3 : {type:String, required:false},
    matchedDocSourceFile3 : {type:String, required:false},
    matchedDocScore3 : {type:Number, required:false},
    
    matchedDocID4 :{type:String, required:false},
    matchedDocTitle4 :{type:String, required:false},
    matchedDocTxt4 : {type:String, required:false},
    matchedDocKeywords4 : {type:String, required:false},
    matchedDocSourceFile4 : {type:String, required:false},
    matchedDocScore4 : {type:Number, required:false},
    
    matchedDocID5 :{type:String, required:false},
    matchedDocTitle5 :{type:String, required:false},
    matchedDocTxt5 : {type:String, required:false},
    matchedDocKeywords5 : {type:String, required:false},
    matchedDocSourceFile5 : {type:String, required:false},
    matchedDocScore5 : {type:Number, required:false},
    
    keywords1:{type:String, required:false},
    keywords2:{type:String, required:false},
    keywords3:{type:String, required:false},
    keywords4:{type:String, required:false},
    keywords5:{type:String, required:false}
   
});


module.exports = mongoose.model('MatchedReportItem', matchedReportItem);
