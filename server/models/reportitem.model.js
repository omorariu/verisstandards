'use strict';


var mongoose = require('mongoose');

var reportItem = new mongoose.Schema({
	
	id: {type:String, required:true},
    doc : {type:String, required:true},
    projectName : {type:String, required:true},
    doc_type : {type:String, required:true},
	type: {type:String, required:false},
    parent_id: {type:String, required:false},
    title: {type:String, required:false},
    direction: {type:String, required:false},
    txt: {type:String, required:false},
    keywords: {type:String, required:false},
    score: {type:Number, required:false},
    match:{type:String, required:false},
    matchID:{type:String, required:false},
    matchTxt:{type:String, required:false},
    matchTitle:{type:String, required:false},
    matchedDocScore : {type:Number, required:false},
    
    matchID2:{type:String, required:false},
    matchTxt2:{type:String, required:false},
    matchTitle2:{type:String, required:false},
    matchedDocScore2 : {type:Number, required:false},
    
    matchID3:{type:String, required:false},
    matchTxt3:{type:String, required:false},
    matchTitle3:{type:String, required:false},
    matchedDocScore3 : {type:Number, required:false},
    
    matchID4:{type:String, required:false},
    matchTxt4:{type:String, required:false},
    matchTitle4:{type:String, required:false},
    matchedDocScore4 : {type:Number, required:false},
    
    matchID5:{type:String, required:false},
    matchTxt5:{type:String, required:false},
    matchTitle5:{type:String, required:false},
    matchedDocScore5 : {type:Number, required:false},
    
    keywords1:{type:String, required:false},
    keywords2:{type:String, required:false},
    keywords3:{type:String, required:false},
    keywords4:{type:String, required:false},
    keywords5:{type:String, required:false},
   
    
    matchdoc:{type:String, required:false},
    matchdoc2:{type:String, required:false},
    matchdoc3:{type:String, required:false},
    matchdoc4:{type:String, required:false},
    matchdoc5:{type:String, required:false} 
});


module.exports = mongoose.model('ReportItem', reportItem);
