'use strict';


var mongoose = require('mongoose');

var settingsSchema = new mongoose.Schema({
	id: {type:String, required:true},
	minParagraphSize: {type: Number, required: true},
	keywordsToConsider: {type: Number, required: true},
	scoreThreshold: {type: Number, required: true},
	excludeList: {type: String, required: false},
	retrieveRankStandardsUsername: {type: String, required: true},
	retrieveRankStandardsPassword: {type: String, required: true},
	retrieveRankStandardsSolrCluster: {type: String, required: true},
	retrieveRankCompanyUsername: {type: String, required: true},
	retrieveRankCompanyPassword: {type: String, required: true},
	retrieveRankCompanySolrCluster: {type: String, required: true},
	documentServiceUsername: {type: String, required: true},
	documentServicePassword: {type: String, required: true},
	alchemyAPIkey: {type: String, required: true},
	enableOneToOneMatch: {type: String, required: true}
});


module.exports = mongoose.model('Settings', settingsSchema);
