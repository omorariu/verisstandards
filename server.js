//Octa M. , 2017 for AIGen

'use strict';

var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    expressValidator = require('express-validator'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    formidable = require('formidable'),
    request = require('request'),
	fs = require('fs'),
    MongoStore = require('connect-mongo/es5')(session),
    sessionDB,
    cfenv = require('cfenv'),
    appEnv = cfenv.getAppEnv(),
    Project = require('./server/models/project.model'),
    Settings = require('./server/models/settings.model'),
    Log = require('./server/models/log.model'),
    IBlock = require('./server/models/iblock.model'),
	ReportItem = require('./server/models/reportitem.model'),
    MatchedReportItem = require('./server/models/matchreportitem.model');
    var DocumentConversionV1 = require('watson-developer-cloud/document-conversion/v1'); 
    var watson = require('watson-developer-cloud');
    
    var files_to_process=0;

    
if(appEnv.isLocal){
    require('dotenv').load();// Loads .env file into environment
}


if(appEnv.isLocal){
    mongoose.connect(process.env.LOCAL_MONGODB_URL);
    sessionDB = process.env.LOCAL_MONGODB_URL;
    console.log('Connected to MongoDB, running at ' + process.env.LOCAL_MONGODB_URL);
}
else if(!appEnv.isLocal) {
    var env = JSON.parse(process.env.VCAP_SERVICES),
        mongoURL = env['mongodb'][0]['credentials']['url'];
    mongoose.connect(mongoURL);
    sessionDB = mongoURL;
    console.log('RUNNIGN IN BULEMIX: Connected to MongoDB, running at ' + mongoURL);
}
else{
    console.log('Unable to connect to MongoDB.');
}




var app = express();
app.enable('trust proxy');


if (!appEnv.isLocal) {
    app.use(function (req, res, next) {
        if (req.secure)
            next();
        else
            res.redirect('https://' + req.headers.host + req.url);
    });
}
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(expressValidator()); 
app.use(cookieParser());



app.get('/', function (req, res){
    res.sendfile('index.html');
});



app.get('/projects/list', function(req,res){
	Project.find({}, function(err, projects) {
		res.send(projects);  
	});
});






app.post('/project/storestd', function(req,res){
	var data = {};
	var form = new formidable.IncomingForm();
	form.uploadDir = "public/pdf";
	form.keepExtensions = true;
	form.parse(req, function(err, fields, files) {
		var dat = JSON.parse(fields.model);
		
	    Project.findOne({ projectName: dat.projectName }, function(err, existingProject) {
	        if (err) {
	            console.log(err);
	            return res.status(400).send('Error updating project.');
	        }
	        
	        var filePath = files.file.path;
	        var fileName = files.file.name;
	        
	        var fileEntry = {};
	        fileEntry.path = filePath;
	        fileEntry.name = fileName;
	        
	        
	        if (existingProject.standardFiles){
	        	existingProject.standardFiles[existingProject.standardFiles.length] = fileEntry;
	        }else{
	        	existingProject.standardFiles = [];
	        	existingProject.standardFiles[0] = fileEntry;
	        }
	        existingProject.save(function(err) {
	            if (err) {
	                console.log(err);
	            }
	            res.end();
	            
	        });
	    });
	});	 
});


app.post('/project/storecomp', function(req,res){
	var data = {};
	var form = new formidable.IncomingForm();
	form.uploadDir = "public/pdf";
	form.keepExtensions = true;
	form.parse(req, function(err, fields, files) {
		var dat = JSON.parse(fields.model);
		Project.findOne({ projectName: dat.projectName }, function(err, existingProject) {
	        if (err) {
	            console.log(err);
	            return res.status(400).send('Error updating project.');
	        }
	        
	        var filePath = files.file.path;
	        var fileName = files.file.name;
	        
	        var fileEntry = {};
	        fileEntry.path = filePath;
	        fileEntry.name = fileName;
	        
	        if (existingProject.companyFiles){
	        	existingProject.companyFiles[existingProject.companyFiles.length] = fileEntry;
	        }else{
	        	existingProject.companyFiles = [];
	        	existingProject.companyFiles[0] = fileEntry;
	        }
	        existingProject.save(function(err) {
	            if (err) {
	                console.log(err);
	            }
	            res.end();
	        });
	    });
	});	 
});	
	




app.post('/project/checkRRS', function(req,res){
		Settings.findOne({ id: '1' }, function(err, settings) {
		    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
		    	var retList = [];
		    	var errorHappened = 0;
		    	var usernameCompany = settings.retrieveRankCompanyUsername;
				var passwordCompany = settings.retrieveRankCompanyPassword;
				var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
				
				var usernameStandards = settings.retrieveRankStandardsUsername;
				var passwordStandards = settings.retrieveRankStandardsPassword;
				var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
					
				var auth = "Basic " + new Buffer(usernameStandards + ":" + passwordStandards).toString("base64");
		        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
				
			    request({
			        url : url,
			        headers : {
			            "Authorization" : auth
			        }
			    },
			    function (error, response, body) {
			    	var rankers = JSON.parse(body).rankers;
			    	if (!error && rankers){
				    	for (var i=0;i<rankers.length;i++){
						    	var ranker_id = rankers[0].ranker_id;
						    	var rank_url = rankers[0].url;
						    	request({
							        url : rank_url,
							        headers : {
							            "Authorization" : auth
							        }
							     },
							     function (error, response, body) {
							    	var rnk = JSON.parse(body);
							    	var r = {};
							    	r.ranker_id = rnk.ranker_id;
							    	r.created = rnk.created;
							    	r.status = rnk.status;
							    	r.status_description = rnk.status_description;
							    	retList[retList.length] = r;
							    });
				    	  }
			    	}else{
			    		errorHappened = 1;
			    		res.end("Error searching for rankers : "  + (error==null?"not found":error));
			    	}
			    });
			    //send response after 5s
			    setTimeout(function(){
			    	console.log(retList);
			    	if (!errorHappened)
			    		res.status(200).send(retList);
			    },2000);
			    
		    });
		});	
});

app.post('/project/deleteRRS', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
	    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	    	var retList = [];
	    	var errorHappened = 0;
	    	var usernameCompany = settings.retrieveRankCompanyUsername;
			var passwordCompany = settings.retrieveRankCompanyPassword;
			var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
			
			var usernameStandards = settings.retrieveRankStandardsUsername;
			var passwordStandards = settings.retrieveRankStandardsPassword;
			var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
				
			var auth = "Basic " + new Buffer(usernameStandards + ":" + passwordStandards).toString("base64");
	        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
			
		    request({
		        url : url,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	var rankers = JSON.parse(body).rankers;
		    	if (!error && rankers){
			    	for (var i=0;i<rankers.length;i++){
					    	var ranker_id = rankers[0].ranker_id;
					    	var rank_url = rankers[0].url;
					    	request.delete({
						        url : rank_url,
						        headers : {
						            "Authorization" : auth
						        }
						     },
						     function (error, response, body) {
						    	 errorHappened = 1;
						    		res.end("Error deleting rankers : "  + (error==null?"not found":error));
						    });
			    	  }
		    	}else{
		    		errorHappened = 1;
		    		res.end("Error deleting rankers : "  + (error==null?"not found":error));
		    	}
		    });
		    //send response after 5s
		    setTimeout(function(){
		    	console.log(retList);
		    	if (!errorHappened)
		    		res.status(200).send("Deleted");
		    },2000);
		    
	    });
	});	
});




app.post('/project/deleteRRV', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
	    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	    	var retList = [];
	    	var errorHappened = 0;
	    	var usernameCompany = settings.retrieveRankCompanyUsername;
			var passwordCompany = settings.retrieveRankCompanyPassword;
			var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
			
			var usernameStandards = settings.retrieveRankStandardsUsername;
			var passwordStandards = settings.retrieveRankStandardsPassword;
			var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
				
			var auth = "Basic " + new Buffer(usernameCompany + ":" + passwordCompany).toString("base64");
	        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
			
		    request({
		        url : url,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	var rankers = JSON.parse(body).rankers;
		    	if (!error && rankers){
			    	for (var i=0;i<rankers.length;i++){
					    	var ranker_id = rankers[0].ranker_id;
					    	var rank_url = rankers[0].url;
					    	request.delete({
						        url : rank_url,
						        headers : {
						            "Authorization" : auth
						        }
						     },
						     function (error, response, body) {
						    	 errorHappened = 1;
						    		res.end("Error deleting rankers : "  + (error==null?"not found":error));
						    });
			    	  }
		    	}else{
		    		errorHappened = 1;
		    		res.end("Error deleting rankers : "  + (error==null?"not found":error));
		    	}
		    });
		    //send response after 5s
		    setTimeout(function(){
		    	console.log(retList);
		    	if (!errorHappened)
		    		res.status(200).send("Deleted");
		    },2000);
		    
	    });
	});	
});









app.post('/project/checkRRV', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
	    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	    	var retList = [];
	    	var errorHappened = 0;
	    	var usernameCompany = settings.retrieveRankCompanyUsername;
			var passwordCompany = settings.retrieveRankCompanyPassword;
			var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
			
			var usernameStandards = settings.retrieveRankStandardsUsername;
			var passwordStandards = settings.retrieveRankStandardsPassword;
			var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
				
			var auth = "Basic " + new Buffer(usernameCompany + ":" + passwordCompany).toString("base64");
	        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
			
		    request({
		        url : url,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	var rankers = JSON.parse(body).rankers;
		    	if (!error && rankers){
			    	for (var i=0;i<rankers.length;i++){
					    	var ranker_id = rankers[0].ranker_id;
					    	var rank_url = rankers[0].url;
					    	request({
						        url : rank_url,
						        headers : {
						            "Authorization" : auth
						        }
						     },
						     function (error, response, body) {
						    	var rnk = JSON.parse(body);
						    	var r = {};
						    	r.ranker_id = rnk.ranker_id;
						    	r.created = rnk.created;
						    	r.status = rnk.status;
						    	r.status_description = rnk.status_description;
						    	retList[retList.length] = r;
						    	
						    });
			    	  }
		    	}else{
		    		errorHappened = 1;
		    		res.end("Error searching for rankers : "  + (error==null?"not found":error));
		    	}
		    });
		    //send response after 5s
		    setTimeout(function(){
		    	if (!errorHappened)
		    		res.status(200).send(retList);
		    },2000);
		    
	    });
	});	
});



app.post('/project/delete', function(req,res){
	    IBlock.find({projectName: req.body.projectName},function(err, iblocks) {
	    }).remove().exec();
	    setTimeout(function(){
	    	Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {res.end("Removed");}).remove().exec();
	    },500);    
});



app.post('/project/run', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	if (existingProject.standardFiles && existingProject.standardFiles.length >0 &&
	        		existingProject.companyFiles && existingProject.companyFiles.length >0){
	        			existingProject.status = 'INIT';
	        	}else{
	        		existingProject.status = 'ERROR: NO_FILES_FOUND';
	        	}
	        }
	        existingProject.save(function(err) {
	            if (err) {
	                console.log(err);
	                res.end();
	                return;
	            }
	            logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	        });
     

        res.end();
    });
});



app.get('/debug', function(req,res){
	Log.find({}, function(err, elogs) {
		var message = '';
    	for (var i=0;i<elogs.length;i++){
    		if (elogs[i].time == null){
    			message += elogs[i].message + '\n';
        			
    		}else{
    			message += elogs[i].time + ' : ' + elogs[i].message + '\n';
    		}
        }
    	return res.status(200).send(message);
    });
});


app.get('/settings', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
        if (!settings) {
        	var settings = new Settings({
        		 id: '1',
	        	 minParagraphSize : 200,
	        	 keywordsToConsider : 5,
	        	 scoreThreshold : 0,
	        	 enableOneToOneMatch : 'yes'
        	});
        	
        	settings.save(function(err) {
        		console.log(err);
        	});
        }
     	return res.status(200).send(settings);
        //res.end();
    });
});

app.post('/settings/save', function(req,res){
	Settings.findOne({ id : '1' }, function(err, existingProject) {}).remove().exec();
	
	var settings = new Settings({
		id : '1',
		minParagraphSize : req.body.minParagraphSize,
		keywordsToConsider : req.body.keywordsToConsider,
		scoreThreshold : req.body.scoreThreshold,
		excludeList : req.body.excludeList,
    	retrieveRankStandardsUsername : req.body.retrieveRankStandardsUsername,
	    retrieveRankStandardsPassword : req.body.retrieveRankStandardsPassword,
	    retrieveRankStandardsSolrCluster : req.body.retrieveRankStandardsSolrCluster,
        retrieveRankCompanyUsername : req.body.retrieveRankCompanyUsername,
	    retrieveRankCompanyPassword : req.body.retrieveRankCompanyPassword,
	    retrieveRankCompanySolrCluster : req.body.retrieveRankCompanySolrCluster,
	    documentServiceUsername : req.body.documentServiceUsername,
	    documentServicePassword : req.body.documentServicePassword,
	    alchemyAPIkey : req.body.alchemyAPIkey,
	    enableOneToOneMatch : req.body.enableOneToOneMatch

    });
	settings.save(function(err) {		
		if (!err){
			res.end("Saved");
		}else{
			res.end("Error saving settings: " + err);
		}
	});
    
});



app.get('/debug/clear', function(req,res){
	
	Log.remove({},function(err){});
	res.end();
    
});




app.get('/reportapi/SVExceptions', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating report for ' + req.query.pName);
		ReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			logMessage("Running S->V Exception : found :" + reportItems.length);
			// check settings
			
			var filtered = [];
			for (var i=0;i<reportItems.length;i++){
				logMessage(reportItems[i].matchID);
				logMessage(reportItems[i].score);
				
				if (reportItems[i].matchID !=null) continue;
				
				if (reportItems[i].score > scoreThreshold){
					var found = false;
					for (var j=0;j<elist.length;j++){
						if (reportItems[i].txt.indexOf(elist[j])>0) {
							found=true;
							logMessage("excluded: " + reportItems[i].id);
						}
					}
					
					if (!found){
						filtered[filtered.length] = reportItems[i];
					}
				}
				
				
			}
			
			res.send(filtered);
		});
	});
	
});


app.get('/reportapi/SVMatches', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating report for ' + req.query.pName);
		ReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			// check settings
			var filtered = [];
			if (reportItems){
				logMessage("Running S->V Match : found :" + reportItems.length);
				for (var i=0;i<reportItems.length;i++){
					logMessage(reportItems[i].matchID);
					logMessage(reportItems[i].score);
					if (reportItems[i].matchID == null) continue;
					
					if (reportItems[i].score > scoreThreshold){
						var found = false;
						for (var j=0;j<elist.length;j++){
							if (reportItems[i].txt.indexOf(elist[j])>0) 
								{ 
								    found=true;
								    logMessage("excluded: " + reportItems[i].id);
								}
						}
						if (!found){
							filtered[filtered.length] = reportItems[i];
						}
					}
				}
			}
			res.send(filtered);
		});
	});
	
});





app.get('/reportsv/getjson', function(req,res){
	var ritems = [];
	Settings.findOne({ id: '1' }, function(err, settings) {
		IBlock.find({doc_type: 'company',projectName:req.query.pName },function (err, iblocks){
				
			ReportItem.find({projectName: req.query.pName},function(err, reportItems) {
				var filtered = [];
				for (var i=0;i<reportItems.length;i++){
					if (reportItems[i].matchID != null){
						reportItems[i].match = 'MATCH';
					}else{
						reportItems[i].match = 'EXCEPTION';
					}
					
					for (var j=0;j<iblocks.length;j++){
						if (iblocks[j].id == reportItems[i].matchID){
							var keywords_str = '';
							for (var k=0;k<iblocks[j].keywords.length;k++){
								keywords_str += iblocks[j].keywords[k].text + ', ';
							}
							
							if (keywords_str.length>0){
								keywords_str.substring(0,keywords_str.length-2);
							}
							reportItems[i].keywords1 = keywords_str;
							reportItems[i].matchdoc = iblocks[j].doc;
						}
						
						if (iblocks[j].id == reportItems[i].matchID2){
							var keywords_str = '';
							for (var k=0;k<iblocks[j].keywords.length;k++){
								keywords_str += iblocks[j].keywords[k].text + ', ';
							}
							
							if (keywords_str.length>0){
								keywords_str.substring(0,keywords_str.length-2);
							}
							reportItems[i].keywords2 = keywords_str;
							reportItems[i].matchdoc2 = iblocks[j].doc;

						}
						
						if (iblocks[j].id == reportItems[i].matchID3){
							var keywords_str = '';
							for (var k=0;k<iblocks[j].keywords.length;k++){
								keywords_str += iblocks[j].keywords[k].text + ', ';
							}
							
							if (keywords_str.length>0){
								keywords_str.substring(0,keywords_str.length-2);
							}
							reportItems[i].keywords3 = keywords_str;
							reportItems[i].matchdoc3 = iblocks[j].doc;

						}
						
						if (iblocks[j].id == reportItems[i].matchID4){
							var keywords_str = '';
							for (var k=0;k<iblocks[j].keywords.length;k++){
								keywords_str += iblocks[j].keywords[k].text + ', ';
							}
							
							if (keywords_str.length>0){
								keywords_str.substring(0,keywords_str.length-2);
							}
							reportItems[i].keywords4 = keywords_str;
							reportItems[i].matchdoc4 = iblocks[j].doc;

						}
						
						if (iblocks[j].id == reportItems[i].matchID5){
							var keywords_str = '';
							for (var k=0;k<iblocks[j].keywords.length;k++){
								keywords_str += iblocks[j].keywords[k].text + ', ';
							}
							
							if (keywords_str.length>0){
								keywords_str.substring(0,keywords_str.length-2);
							}
							reportItems[i].keywords5 = keywords_str;
							reportItems[i].matchdoc5 = iblocks[j].doc;

						}
						
					}
					
					
				}
				
				
				
				
				
				
				res.send(reportItems);
			});
		});
		
	});	
});



app.get('/reportvs/getjson', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		IBlock.find({doc_type: 'standards',projectName:req.query.pName },function (err, iblocks){
				MatchedReportItem.find({projectName: req.query.pName},function(err, reportItems) {
					logMessage('V-S report items:' + reportItems.length);
					for (var i=0;i<reportItems.length;i++){
						if (reportItems[i].matchedDocID != null){
							reportItems[i].match = 'MATCH';
						}else{
							reportItems[i].match = 'EXCEPTION';
						}
						
						for (var j=0;j<iblocks.length;j++){
							if (iblocks[j].id == reportItems[i].matchedDocID){
								var keywords_str = '';
								for (var k=0;k<iblocks[j].keywords.length;k++){
									keywords_str += iblocks[j].keywords[k].text + ', ';
								}
								
								if (keywords_str.length>0){
									keywords_str.substring(0,keywords_str.length-2);
								}
								reportItems[i].keywords1 = keywords_str;
								reportItems[i].matchdoc = iblocks[j].doc;

							}
							
							if (iblocks[j].id == reportItems[i].matchedDocID2){
								var keywords_str = '';
								for (var k=0;k<iblocks[j].keywords.length;k++){
									keywords_str += iblocks[j].keywords[k].text + ', ';
								}
								
								if (keywords_str.length>0){
									keywords_str.substring(0,keywords_str.length-2);
								}
								reportItems[i].keywords2 = keywords_str;
								reportItems[i].matchdoc2 = iblocks[j].doc;

							}
							
							if (iblocks[j].id == reportItems[i].matchedDocID3){
								var keywords_str = '';
								for (var k=0;k<iblocks[j].keywords.length;k++){
									keywords_str += iblocks[j].keywords[k].text + ', ';
								}
								
								if (keywords_str.length>0){
									keywords_str.substring(0,keywords_str.length-2);
								}
								reportItems[i].keywords3 = keywords_str;
								reportItems[i].matchdoc3 = iblocks[j].doc;

							}
							
							if (iblocks[j].id == reportItems[i].matchedDocID4){
								var keywords_str = '';
								for (var k=0;k<iblocks[j].keywords.length;k++){
									keywords_str += iblocks[j].keywords[k].text + ', ';
								}
								
								if (keywords_str.length>0){
									keywords_str.substring(0,keywords_str.length-2);
								}
								reportItems[i].keywords4 = keywords_str;
								reportItems[i].matchdoc4 = iblocks[j].doc;

							}
							
							if (iblocks[j].id == reportItems[i].matchedDocID5){
								var keywords_str = '';
								for (var k=0;k<iblocks[j].keywords.length;k++){
									keywords_str += iblocks[j].keywords[k].text + ', ';
								}
								
								if (keywords_str.length>0){
									keywords_str.substring(0,keywords_str.length-2);
								}
								reportItems[i].keywords5 = keywords_str;
								reportItems[i].matchdoc5 = iblocks[j].doc;

							}
						}
						
					}
					res.send(reportItems);
				});
		});
	});
});





app.get('/getIndicators', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		console.log('Getting iblocks for ' + req.query.pName);
		IBlock.find({projectName: req.query.pName},function(err, iblocks) {
			var filtered = [];
			for (var i=0;i<iblocks.length;i++){
				if (iblocks[i].doc_type=='standards'){
					filtered[filtered.length]=iblocks[i];
				}
			}
			res.send(filtered);
		});
	});
});

app.get('/getVerisIndicators', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		console.log('Getting iblocks for ' + req.query.pName);
		IBlock.find({projectName: req.query.pName},function(err, iblocks) {
			var filtered = [];
			for (var i=0;i<iblocks.length;i++){
				var iblk = iblocks[i];
				if (iblk.doc_type=='company'){
					//for (var k=0;k<iblk.keywords.length;k++){
						
					//	iblk.kwords +=iblk.keywords[k].text + ", ";
					//}
					//console.log(iblk.kwords);
					filtered[filtered.length]=iblk;
					//console.log(filtered[filtered.length-1].kwords);
					
				}
			}
			res.send(filtered);
		});
	});
});




app.get('/removeIndicator', function(req,res){
	console.log('Removing iblocks for ' + req.query.id);
	IBlock.find({id: req.query.id},function(err, iblock) {}).remove().exec();
	res.end();
});


app.get('/reportapi/VSMatches', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating match report for ' + req.query.pName);
		MatchedReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			// check settings
			var filtered = [];
			for (var i=0;i<reportItems.length;i++){
				if (reportItems[i].score > scoreThreshold){
					var found = false;
					for (var j=0;j<elist.length;j++){
						if (reportItems[i].txt.indexOf(elist[j])>0) found=true;
					}
					if (!found){
						filtered[filtered.length] = reportItems[i];
					}
				}
			}
			res.send(filtered);
		});
	});
});


app.get('/reportmatchx2', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating match report for ' + req.query.pName);
		MatchedReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			// check settings
			var filtered = [];
			for (var i=0;i<reportItems.length;i++){
				/*if (reportItems[i].score > scoreThreshold){
					var found = false;
					for (var j=0;j<elist.length;j++){
						if (reportItems[i].txt.indexOf(elist[j])>0) found=true;
					}
					if (!found && reportItems[i].txt.length> 400){
						filtered[filtered.length] = reportItems[i];
					}
				}*/
				if (reportItems[i].matchedDocID == null){
					filtered[filtered.length] = reportItems[i];
				}
			
			}
			res.send(filtered);
		});
	});
});




app.get('/reportapi/VSMatches', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating match report for ' + req.query.pName);
		MatchedReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			// check settings
			var filtered = [];
			for (var i=0;i<reportItems.length;i++){
				if (reportItems[i].matchedDocID == null) continue;
				if (reportItems[i].score > scoreThreshold){
					var found = false;
					for (var j=0;j<elist.length;j++){
						if (reportItems[i].txt.indexOf(elist[j])>0) found=true;
					}
					if (!found){
						filtered[filtered.length] = reportItems[i];
					}
				}
			}
			/////////////////////////
			logMessage("running V2 match report");
			var filtered2 = [] , matchedList = [];
			///////////hack/////////////////////////
			if (settings.enableOneToOneMatch == 'yes' ||settings.enableOneToOneMatch == 'Yes' || settings.enableOneToOneMatch == 'YES'){
					var alreadyMatched = {};
					for (var j = 0;j < filtered.length; j++){
						//avoid same match multiple
						var ri = filtered[j];
						var Title, Txt, ID;
						if (!alreadyMatched[ri.matchedDocID]){
							alreadyMatched[ri.matchedDocID] = 1;
							logMessage('not matched yet, adding to list');
							filtered2[filtered2.length] = ri;
						}else if (!alreadyMatched[ri.matchedDocID2]){
							alreadyMatched[ri.matchedDocID2] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile2;
							ri.matchedDocTxt = ri.matchedDocTxt2;
							ri.matchedDocID = ri.matchedDocID2;
							
							ri.matchedDocTile2 = Title;
							ri.matchedDocTxt2 = Txt;
							ri.matchedDocID2 = ID;
							ri.matchedDocTile2 = null;
							ri.matchedDocTxt2 = null;
							ri.matchedDocID2 = null;
							
							logMessage('not matched 2, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else if (!alreadyMatched[ri.matchedDocID3]){
							alreadyMatched[ri.matchedDocID3] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile3;
							ri.matchedDocTxt = ri.matchedDocTxt3;
							ri.matchedDocID = ri.matchedDocID3;
							ri.matchedDocTile3 = Title;
							ri.matchedDocTxt3 = Txt;
							ri.matchedDocID3 = ID;
		
							ri.matchedDocTile3 = null;
							ri.matchedDocTxt3 = null;
							ri.matchedDocID3 = null;
							logMessage('not matched 3, adding to list');
							filtered2[filtered2.length] = ri;
						}else if (!alreadyMatched[ri.matchedDocID4]){
							alreadyMatched[ri.matchedDocID4] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile4;
							ri.matchedDocTxt = ri.matchedDocTxt4;
							ri.matchedDocID = ri.matchedDocID4;
							ri.matchedDocTile4 = Title;
							ri.matchedDocTxt4 = Txt;
							ri.matchedDocID4 = ID;
		
							ri.matchedDocTile4 = null;
							ri.matchedDocTxt4 = null;
							ri.matchedDocID4 = null;
							filtered[j]= ri;
							logMessage('not matched 4, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else if (!alreadyMatched[ri.matchedDocID5]){
							alreadyMatched[ri.matchedDocID5] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile5;
							ri.matchedDocTxt = ri.matchedDocTxt5;
							ri.matchedDocID = ri.matchedDocID5;
							ri.matchedDocTile5 = Title;
							ri.matchedDocTxt5 = Txt;
							ri.matchedDocID5 = ID;
		
							ri.matchedDocTile5 = null;
							ri.matchedDocTxt5 = null;
							ri.matchedDocID5 = null;
							filtered[j]= ri;
							logMessage('not matched 5, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else{
							//logMessage('All 5 match , skipping');
							//filtered2[filtered2.length] = ri;
							
						}
				
						}
						/////////////end hack//////////////////
					    matchedList = filtered2;
			}else{
				matchedList = filtered;
			}
			
			IBlock.find({doc_type: 'standards',projectName:req.query.pName },function (err, iblocks){
				var returnList = [];
				for (var i=0;i<iblocks.length;i++){
					var iblock = iblocks[i];
					var found = 0;
					for (var k=0;k<matchedList.length;k++){
						if (matchedList[k].matchedDocID == iblock.id || 
							matchedList[k].matchedDocID2 == iblock.id || 
							matchedList[k].matchedDocID3 == iblock.id || 
							matchedList[k].matchedDocID4 == iblock.id || 
							matchedList[k].matchedDocID5 == iblock.id)
						found = 1;
					}
					if (!found){
						returnList[returnList.length]= iblock;
					}
				}
				res.send(returnList);
			});
		});
	});
});





app.get('/reportmatchv2', function(req,res){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var scoreThreshold = settings.scoreThreshold;
	    var excludeList = settings.excludeList;
	    var elist = [];
	    if (excludeList.indexOf(',')>0){
	    	elist = excludeList.split(',');
	    }
		console.log('Generating match report for ' + req.query.pName);
		MatchedReportItem.find({projectName: req.query.pName},function(err, reportItems) {
			// check settings
			var filtered = [];
			for (var i=0;i<reportItems.length;i++){
				if (reportItems[i].matchedDocID == null) continue;
				if (reportItems[i].score > scoreThreshold){
					var found = false;
					for (var j=0;j<elist.length;j++){
						if (reportItems[i].txt.indexOf(elist[j])>0) found=true;
					}
					if (!found){
						filtered[filtered.length] = reportItems[i];
					}
				}
			}
			/////////////////////////
			logMessage("running V2 match report");
			var filtered2 = [];
			///////////hack/////////////////////////
			if (settings.enableOneToOneMatch == 'yes' ||settings.enableOneToOneMatch == 'Yes' || settings.enableOneToOneMatch == 'YES'){
					var alreadyMatched = {};
					for (var j = 0;j < filtered.length; j++){
						//avoid same match multiple
						var ri = filtered[j];
						var Title, Txt, ID;
						if (!alreadyMatched[ri.matchedDocID]){
							alreadyMatched[ri.matchedDocID] = 1;
							logMessage('not matched yet, adding to list');
							filtered2[filtered2.length] = ri;
						}else if (!alreadyMatched[ri.matchedDocID2]){
							alreadyMatched[ri.matchedDocID2] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile2;
							ri.matchedDocTxt = ri.matchedDocTxt2;
							ri.matchedDocID = ri.matchedDocID2;
							
							ri.matchedDocTile2 = Title;
							ri.matchedDocTxt2 = Txt;
							ri.matchedDocID2 = ID;
							ri.matchedDocTile2 = null;
							ri.matchedDocTxt2 = null;
							ri.matchedDocID2 = null;
							
							logMessage('not matched 2, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else if (!alreadyMatched[ri.matchedDocID3]){
							alreadyMatched[ri.matchedDocID3] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile3;
							ri.matchedDocTxt = ri.matchedDocTxt3;
							ri.matchedDocID = ri.matchedDocID3;
							ri.matchedDocTile3 = Title;
							ri.matchedDocTxt3 = Txt;
							ri.matchedDocID3 = ID;
		
							ri.matchedDocTile3 = null;
							ri.matchedDocTxt3 = null;
							ri.matchedDocID3 = null;
							logMessage('not matched 3, adding to list');
							filtered2[filtered2.length] = ri;
						}else if (!alreadyMatched[ri.matchedDocID4]){
							alreadyMatched[ri.matchedDocID4] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile4;
							ri.matchedDocTxt = ri.matchedDocTxt4;
							ri.matchedDocID = ri.matchedDocID4;
							ri.matchedDocTile4 = Title;
							ri.matchedDocTxt4 = Txt;
							ri.matchedDocID4 = ID;
		
							ri.matchedDocTile4 = null;
							ri.matchedDocTxt4 = null;
							ri.matchedDocID4 = null;
							filtered[j]= ri;
							logMessage('not matched 4, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else if (!alreadyMatched[ri.matchedDocID5]){
							alreadyMatched[ri.matchedDocID5] = 1;
							Title = ri.matchedDocTile;
							Txt = ri.matchedDocTxt;
							ID = ri.matchedDocID;
							ri.matchedDocTile = ri.matchedDocTile5;
							ri.matchedDocTxt = ri.matchedDocTxt5;
							ri.matchedDocID = ri.matchedDocID5;
							ri.matchedDocTile5 = Title;
							ri.matchedDocTxt5 = Txt;
							ri.matchedDocID5 = ID;
		
							ri.matchedDocTile5 = null;
							ri.matchedDocTxt5 = null;
							ri.matchedDocID5 = null;
							filtered[j]= ri;
							logMessage('not matched 5, adding to list');
							filtered2[filtered2.length] = ri;
						
						}else{
							//logMessage('All 5 match , skipping');
							//filtered2[filtered2.length] = ri;
							
						}
				
						}
						/////////////end hack//////////////////
						res.send(filtered2);
			}else{
				res.send(filtered);
			}
		});
	});
});



app.post('/project/new', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    	projectDesc: req.body.projectDesc,
    	alchemyCustomModel: req.body.alchemyCustomModel,
    	status: 'NEW'
    });
    Project.findOne({$or:[{ projectName: req.body.projectName },{status: {"$ne": "PROJECT CLOSED"}}]}, function(err, existingProject) {
        if (existingProject) {
        	if (existingProject.projectName == req.body.projectName){
        		return res.status(400).send('That project name already exists. Please try a different project name.');
        	}
        	if (existingProject.status != "PROJECT CLOSED"){
        		return res.status(400).send('Please make sure all other projects are in "PROJECT CLOSED" state before you create a new project!');
        	}
        
        }
        
        
        project.save(function(err) {
            if (err) {
                console.log(err);
                res.status(500).send('Error saving new project (database error). Please try again.');
                return;
            }
            logMessage('Created project ' + req.body.projectName);
            res.status(200).send('Project created and execution started! Please check status in My Reports view.');
        });
        
        
    });

});



app.post('/project/digestStandardFiles', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	            startStandardToIBlocks(res,existingProject);
			       
	        	/*if (existingProject.status == 'INIT'){
	        		existingProject.status = 'DIGESTING STANDARD FILES';
	        	       existingProject.save(function(err) {
		       	            if (err) {
		       	                console.log(err);
		       	                res.end();
		       	                return;
		       	            }
		       	    	    logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	       	           });    	   
	        	}*/
	        }
        //res.end();
    });
});




app.post('/project/executeCrossSearch', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	//if (existingProject.projectName!='Veris Project'){
	        	//	res.end('Only enabled for Veris Project at this time');
	        	//}else{
	              executeCrossSearch(res, existingProject);
	        	//}
	        }
       // res.end('Data will be ready in a few seconds.');
    });
});

app.post('/project/executeCrossMatchSearch', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	//if (existingProject.projectName!='Veris Project'){
	        	//	res.end('Only enabled for Veris Project at this time');
	        	//}else{
	              executeCrossSearchMatch(res, existingProject);
	        	//}
	        }
       // res.end('Data will be ready in a few seconds.');
    });
});


app.post('/project/releaseResources', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	
	        	//if (existingProject.status == 'CROSS SEARCH EXECUTED'){
	        	releaseResources(res, existingProject);
	        	//}
	        }
            res.end("Deleted rankers and data from SOLR clusters. Report will run from local copy of data.");
        
    });
});




app.post('/project/digestCompanyFiles', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	            startCompanyToIBlocks(res, existingProject);
			       
	        	/*if (existingProject.status == 'PROCESS STANDARD FILES DONE'){
	        		existingProject.status = 'DIGESTING COMPANY FILES';
	        	       existingProject.save(function(err) {
		       	            if (err) {
		       	                console.log(err);
		       	                res.end();
		       	                return;
		       	            }
		       	            startCompanyToIBlocks(res, existingProject);
			        	    logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	       	           });    	   
	        	}*/
	        }
       // res.end();
    });
});




app.post('/project/extractEntities', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	
	        	//if (existingProject.status == 'PROCESS VERIS FILES DONE'){
	        		existingProject.status = 'EXTRACTING ENTITIES (ALCHEMY)';
	        	       existingProject.save(function(err) {
		       	            if (err) {
		       	                console.log(err);
		       	                res.end();
		       	                return;
		       	            }
		       	            startAlchemyKeywordsExtraction(res, existingProject);
			        	    logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	       	           });    	   
	        	//}
	        }
        //res.end();
    });
});







app.post('/project/createSolrClusterStandards', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	
	        	if (existingProject.status == 'EXTRACTING ENTITIES (ALCHEMY) DONE'){
	        		existingProject.status = 'CREATING SORL CLUSTER STANDARDS';
	        	       existingProject.save(function(err) {
		       	            if (err) {
		       	                console.log(err);
		       	                res.end();
		       	                return;
		       	            }
		       	            createSolrClusterStandards(existingProject);
			        	    logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	       	           });    	   
	        	}
	        }
        res.end();
    });
});



app.post('/project/createSolrClusterCompany', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	
	        	if (existingProject.status == 'STANDARDS SOLR CLUSTER CREATED'){
	        		existingProject.status = 'CREATING SORL CLUSTER COMPANY';
	        	       existingProject.save(function(err) {
		       	            if (err) {
		       	                console.log(err);
		       	                res.end();
		       	                return;
		       	            }
		       	            createSolrClusterCompany(existingProject);
			        	    logMessage('Project ' + req.body.projectName + ' now in status ' + existingProject.status);
	       	           });    	   
	        	}
	        }
        res.end();
    });
});



app.post('/project/test', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	 testFunction(existingProject);
	        }
        res.end();
    });
});

app.post('/project/generateTrainingDataCompany', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	              generateTrainingDataCompany(existingProject);
			}
        res.end('Check Debug screen for training data');
    });
});


app.post('/project/generateTrainingDataStandards', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName,
    });
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	generateTrainingDataStandards(existingProject);
	        }
        res.end('Check Debug screen for training data');
    });
});



app.post('/project/loadDataStandard', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName
    });
    
    console.log(req.body.projectName);
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	//if (existingProject.projectName!= 'Veris Project' ){
	        	//	logMessage("Cannot load standards data for " + existingProject.projectName + " as it will corrupt the reference project!");
	        	//}else{
	        		loadDataStandard(res, existingProject);
	        	//}
	        }
        //res.end();
    });
});


app.post('/project/loadDataCompany', function(req,res){
    req.checkBody('projectName', 'Project Name is required').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }
    var project = new Project({
    	projectName: req.body.projectName
    });
    
    console.log(req.body.projectName);
    
    Project.findOne({ projectName: req.body.projectName }, function(err, existingProject) {
	        if (existingProject) {
	        	
	        	if (existingProject) {
		        	//if (existingProject.projectName!= 'Veris Project' ){
		        	//	logMessage("Cannot load data for " + existingProject.projectName + " as it will corrupt the reference project!");
		        	//}else{
		        		loadDataCompany(res, existingProject);
			        //}
		        }
	        	
	        
	        }
       //res.end();
    });
});



function loadDataStandard(res, project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankStandardsUsername;
		var password = settings.retrieveRankStandardsPassword;
		var solr_cluster = settings.retrieveRankStandardsSolrCluster;
		
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

		IBlock.find({$and:[{doc_type:'standards'},{projectName:project.projectName}]} ,function(err, iblocks){
			//console.log(iblocks.length);
			var json_data = '{';
			for (var i=0;i<iblocks.length;i++){
				 var data = {};
			     var docu = {};
			     var addition = {};
			     docu.id = iblocks[i].id;
			     //docu.author = iblocks[i].doc;
			     //docu.bibliography = iblocks[i].doc;
			     docu.body = iblocks[i].content[0].text;
			     docu.title = iblocks[i].title;
			     addition.doc = docu;
			     //data.add = addition;
			     json_data += "\"add\" : "; 
			     	
			     json_data += JSON.stringify(addition);
			     json_data += "";
			     if (i<iblocks.length-1){
			    	 json_data+=',';
			     }
			}
			json_data += '}';
			// console.log(json_data);
			 var url_collections = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solr_cluster + '/solr/std_solr_config_collection/update';
  			 //console.log(url_collections);
			 var req = request.post({
  	    	        url : url_collections,
  	    	        headers : {
  	    	            "Authorization" : auth,
  	    	            "content-type" : "application/json"
  	    	        },
  	    	        body: json_data
  	         },function (err, resp, body) {
  				  if (err){
  					  console.log("Error adding document.")
  				  }else{

  					  setProjectStatus(project.projectName, 'LOADED STANDARD DOCUMENTS');
  					  console.log(body);
  					  logMessage("Successfully loaded documents to " + solr_cluster);
  					  res.end("Successfully loaded " + iblocks.length + " information blocks to " + solr_cluster+ ". Please allow 5 minutes for the indexing to complete.");
  				  }
  			 });	
		
		});
	});
}



function loadDataCompany(res, project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankCompanyUsername;
		var password = settings.retrieveRankCompanyPassword;
		var solr_cluster = settings.retrieveRankCompanySolrCluster;
		
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

		IBlock.find({$and:[{doc_type:'company'},{projectName:project.projectName}]},function(err, iblocks){
			var json_data = '{';
			for (var i=0;i<iblocks.length;i++){
				 var data = {};
			     var docu = {};
			     var addition = {};
			     docu.id = iblocks[i].id;
			     //docu.author = iblocks[i].doc;
			     //docu.bibliography = iblocks[i].doc;
			     docu.body = iblocks[i].content[0].text;
			    
			     docu.title = iblocks[i].title;
			     addition.doc = docu;
			     //data.add = addition;
			     json_data += "\"add\" : "; 
			     	
			     json_data += JSON.stringify(addition);
			     json_data += "";
			     if (i<iblocks.length-1){
			    	 json_data+=',';
			     }
			}
			json_data += '}';
			var url_collections = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solr_cluster + '/solr/std_solr_config_collection/update';
			//console.log(url_collections);
			//console.log(json_data);
			var req = request.post({
  	    	        url : url_collections,
  	    	        headers : {
  	    	            "Authorization" : auth,
  	    	            "content-type" : "application/json"
  	    	        },
  	    	        body: json_data
  	         },function (err, resp, body) {
  				  if (err){
  					  console.log("Error adding document.")
  				  }else{
  					  console.log(body);

  					  setProjectStatus(project.projectName, 'LOADED VERIS DOCUMENTS');
  					  logMessage("Successfully loaded documents ");
  					  res.end("Successfully loaded " + iblocks.length + " information blocks to " + solr_cluster + ". Please allow 5 minutes for the indexing to complete.");
  				  }
  			 });	
		});
	});
}




function logMessage(msg){
	var logger = new Log({
		id: new Date().getTime(),
		time: new Date(),
		message: msg
	});
	logger.save(function(err) {
            if (err) {
                console.log('logger error:'  + err);
                return;
            }
       
    });
}

function logMessageDirect(msg){
	var logger = new Log({
		id: new Date().getTime(),
		time: null,
		message: msg
	});
	logger.save(function(err) {
            if (err) {
                console.log('logger error:'  + err);
                return;
            }
       
    });
}


function createSolrClusterStandards(project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankStandardsUsername;
		var password = settings.retrieveRankStandardsPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters";
		
		request.post({
        	url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	console.log(JSON.parse(body).solr_cluster_id);
	    	var solr_id = JSON.parse(body).solr_cluster_id;
	    	
	    	
	    	Project.findOne({ projectName: project.projectName }, function(err, existingProject) {
		        if (existingProject) {
		        	
		        	existingProject.solrClusterStandards = solr_id;
		        	existingProject.save(function(err) {
			       	            if (err) {
			       	                console.log(err);
			       	                res.end();
			       	                return;
			       	            }
			       	            checkClusterStandardStatus(project.projectName, solr_id);
			       	         
		       	    });    	   
		        	
		        }
	      
	  
	    	});
	    	
	    });		
	});
}


function checkClusterStandardStatus(pName, solr_cluster_id){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankStandardsUsername;
		var password = settings.retrieveRankStandardsPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solr_cluster_id;
		
	    request({
	        url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	
	    	var status = JSON.parse(body).solr_cluster_status;
	    	logMessage('Checking SOLR cluster ' + solr_cluster_id + '. Status is:' + status );
	    	
	    	
	    	if (status=='READY'){
	    		Project.findOne({ projectName: pName }, function(err, existingProject) {
	    	        if (existingProject) {
	    	        		existingProject.status = 'STANDARDS SOLR CLUSTER CREATED';
	    	        	       existingProject.save(function(err) {
	    		       	            if (err) {
	    		       	                console.log(err);
	    		       	                res.end();
	    		       	                return;
	    		       	            }
	    			        	    logMessage('Project ' + pName + ' now in status ' + existingProject.status);
	    	       	           });    	   
	    	        	
	    	        }
	       
	    	        createConfigAndCollectionStandard(existingProject);
	    	        
	    	        
	    		});
	    		
	    		 
	    		
	    		
	    	}else{
	    		setTimeout(	checkClusterStandardStatus(pName, solr_cluster_id), 10000);
	    	}
	    });		
	});
}






function createConfigAndCollectionStandard(project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankStandardsUsername;
		var password = settings.retrieveRankStandardsPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + project.solrClusterStandards;
		
	    Project.findOne({ projectName:  project.projectName }, function(err, existingProject) {
	    	        if (existingProject) {
	    	        	
	    	        	var url_config = url + '/config/std_solr_config';
	    	        	
	    	        	var req = request.post({
	    	    	        url : url_config,
	    	    	        headers : {
	    	    	            "Authorization" : auth
	    	    	        },
	    	    	        body: fs.createReadStream('std_solr_config.zip')
	    	    	        	
	    	        	}, function (err, resp, body) {
	    	        		  if (err) {
	    	        		    console.log('Error!');
	    	        		  } else {
	    	        			  
	    	        			 
	    	       	        	var url_collections = url + '/solr/admin/collections';
	    	       	            console.log(url_collections); 
	    	       	        	// "action=CREATE&name=example_collection&collection.configName=example_config"
		   	        			  var req = request.post({
		   		    	    	        url : url_collections,
		   		    	    	        headers : {
		   		    	    	            "Authorization" : auth,
		   		    	    	            "content-type" : "application/x-www-form-urlencoded"
		   		    	    	        },
		   		    	    	        body: "action=CREATE&name=std_solr_config_collection&collection.configName=std_solr_config"
		   		    	        	  
		   	        			  },function (err, resp, body) {
		   	        				  if (err){
		   	        					  console.log("Error creating collection.")
		   	        				  }else{
		   	        					  console.log("collection : "  + body);
		   	        					  console.log("collection created");
		   	        					  logMessage("Successfully created collection std_solr_config_collection for project " + existingProject.projectName);
		   	        				  }
		   	        			  });
		   	        		  }
	    	        	});
	    	        }
	    });		
	});
}



function createConfigAndCollectionCompany(project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankCompanyUsername;
		var password = settings.retrieveRankCompanyPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + project.solrClusterCompany;
		
	    Project.findOne({ projectName:  project.projectName }, function(err, existingProject) {
	    	        if (existingProject) {
	    	        	
	    	        	var url_config = url + '/config/std_solr_config';
	    	        	
	    	        	var req = request.post({
	    	    	        url : url_config,
	    	    	        headers : {
	    	    	            "Authorization" : auth
	    	    	        },
	    	    	        body: fs.createReadStream('std_solr_config.zip')
	    	    	        	
	    	        	}, function (err, resp, body) {
	    	        		  if (err) {
	    	        		    console.log('Error!');
	    	        		  } else {
	    	        			  
	    	        			  
	    	       	        	var url_collections = url + '/solr/admin/collections';
	    	       	        	console.log(url_collections); 
	    	       	        	
	    	       	        	
		   	        			  var req = request.post({
		   		    	    	        url : url_collections,
		   		    	    	        headers : {
		   		    	    	            "Authorization" : auth,
		   		    	    	            "content-type" : "application/x-www-form-urlencoded"
		   		    	    	        },
		   		    	    	        body: "action=CREATE&name=std_solr_config_collection&collection.configName=std_solr_config"
		   		    	        	  
		   	        			  },function (err, resp, body) {
		   	        				  if (err){
		   	        					  console.log("Error creating collection.")
		   	        				  }else{
		   	        					  console.log("colection created");
		   	        					  logMessage("Successfully created collection std_solr_config_collection for project " + existingProject.projectName);
		   	        				  }
		   	        			  });
		   	        		  }
	    	        	});
	    	        }
	    });		
	});
}







function createSolrClusterCompany(project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankCompanyUsername;
		var password = settings.retrieveRankCompanyPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters";
		
        request.post({
        	
	        url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	console.log(JSON.parse(body).solr_cluster_id);
	    	var solr_id = JSON.parse(body).solr_cluster_id;
	    	
	    	
	    	Project.findOne({ projectName: project.projectName }, function(err, existingProject) {
		        if (existingProject) {
		        	
		        	existingProject.solrClusterCompany = solr_id;
		        	existingProject.save(function(err) {
			       	            if (err) {
			       	                console.log(err);
			       	                res.end();
			       	                return;
			       	            }
			       	            checkClusterCompanyStatus(project.projectName, solr_id);
			       	         
		       	    });    	   
		        	
		        }
	      
	  
	    	});
	    	
	    });		
	});
}








function checkClusterCompanyStatus(pName, solr_cluster_id){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankCompanyUsername;
		var password = settings.retrieveRankCompanyPassword;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solr_cluster_id;
		
	    request({
	        url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	
	    	var status = JSON.parse(body).solr_cluster_status;
	    	logMessage('Checking SOLR cluster ' + solr_cluster_id + '. Status is:' + status );
	    	if (status=='READY'){
	    		Project.findOne({ projectName: pName }, function(err, existingProject) {
	    	        if (existingProject) {
	    	        		existingProject.status = 'COMPANY SOLR CLUSTER CREATED';
	    	        	       existingProject.save(function(err) {
	    		       	            if (err) {
	    		       	                console.log(err);
	    		       	                res.end();
	    		       	                return;
	    		       	            }
	    			        	    logMessage('Project ' + pName + ' now in status ' + existingProject.status);
	    	       	           });    	   
	    	        	
	    	        }
	    	        createConfigAndCollectionCompany(existingProject);
	    		});
	    		
	    		
	    	}else{
	    		setTimeout(	checkClusterCompanyStatus(pName, solr_cluster_id), 10000);
	    	}
	    });		
	});
}
















function postFileToDocumentService(file,ftype,pName){
	Settings.findOne({ id: '1' }, function(err, settings) {
	    var documentConversion = new DocumentConversionV1({
	    	  username: settings.documentServiceUsername,
	    	  password: settings.documentServicePassword,
	    	  version_date: '2015-12-01',
	    }); 
	    const filep = file.path;
	    const params = {
	      conversion_target: 'answer_units',
	      file: filep ? fs.createReadStream(filep) : null,
	    };
	    documentConversion.convert(params, function(err, data) {
	      if (err) {
	    	  logMessage('Error processing file ' + file.name + '. Error is: '+err);
	      }else{
	    	  logMessage('File ' + file.name + ' generated ' + data.answer_units.length + ' information blocks');
	    	  //console.log(data.answer_units.length);
	    	  for (var i=0;i<data.answer_units.length;i++){
	    		  var iblock = data.answer_units[i];
	    		  if (iblock.content[0].text.length >  settings.minParagraphSize){
	    			  
	    			  
	    			  var idx = iblock.content[0].text.indexOf('Indicator ID:');
	    			  if (idx >0 ){
	    				  iblock.content[0].text = iblock.content[0].text.substring(idx, iblock.content[0].text.length);
	    			  }
	    			  
	    			  if (iblock.title=='no-title' && iblock.content[0].text.length>23){
	 			    	 iblock.title = iblock.content[0].text.substring(0,22);
	 			       }
	    			  
	    			  iblock.doc = file.name;
	    			  iblock.doc_type = ftype;
	    			  iblock.projectName = pName;
	    			  
	    		  
		    		  var ib = new IBlock(iblock);
		    		  ib.save(
		    				  function(err) {
		    			            if (err) {
		    			                console.log('iblock save err:  ' + err);
		    			                return;
		    			            }else{
		    			            	// console.log('iblock saved');
		    			            }
		    		   });  
	    		  }
	    	  }
	    	  setTimeout(function (){files_to_process--;},500);
	      }
	    }); 	    
	});
	
}


/////////////////////////AI LOGIC//////////////////////////////

function processFilesStandards(res, pName, files, index){
	
	if (files[index]){
		logMessage('Processing ' + files[index].name);
		
		postFileToDocumentService(files[index],'standards',pName);
		
		logMessage('Processed ' + files[index].name);
		
		//call Doc ///
		index++;
		setTimeout(processFilesStandards.bind(null,res, pName, files, index),200);
	}else{
		setProjectStatus(pName, 'PROCESS STANDARD FILES DONE');
		res.end(files.length + " files in submitted! Check log screen for progress.");
	}
}


function processFilesCompany(res, pName, files, index){

	if (files[index]){

		logMessage('Processing ' + files[index].name);
		postFileToDocumentService(files[index],'company',pName);
		logMessage('Processed ' + files[index].name);
		
		//call Doc ///
		index++;
		
		setTimeout(processFilesCompany.bind(null,res, pName, files, index), 200);
	}else{
		setProjectStatus(pName, 'PROCESS VERIS FILES DONE');
		res.end(files.length + " files in submitted! Check log screen for progress.");
	}
}

function startStandardToIBlocks(res, project){
	logMessage('PHASE 1: Staring Standard Document Conversion for ' + project.projectName + ' size:' +  project.standardFiles.length );
	files_to_process +=  project.standardFiles.length;
	processFilesStandards(res, project.projectName, project.standardFiles,0);
}

function startCompanyToIBlocks(res, project){
	logMessage('PHASE 2: Staring Company Document Conversion for ' + project.projectName + ' size:' +  project.companyFiles.length);
	files_to_process +=  project.companyFiles.length;
	processFilesCompany(res, project.projectName, project.companyFiles,0);
}



function startAlchemyKeywordsExtraction(res, project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var alchemyLanguage = watson.alchemy_language({
			  api_key: settings.alchemyAPIkey
			}); 
		
		
		IBlock.find({projectName: project.projectName} , function(err, iblocks) {
			iblocks.forEach(function(iblock) {
					
					/// invoke alchemy
				    var params = {};
				    //params.sentiment = 1;
				    params.text = iblock.content[0].text;
					alchemyLanguage['keywords'](params, function(err, response) {
				      if (err) {
				        console.log('alchemy invoke error = ' + err);
				        logMessage('Alchemy invoke error:' + err);
				      }else{
				    	  var keylist = [];
				    	  var limit = 0;
				    	  if (response.keywords.length>settings.keywordsToConsider){
				    		  limit = settings.keywordsToConsider;
				    	  }else{
				    		  limit = response.keywords.length;
				    	  }
				    	  for (var i=0;i<limit;i++){
				    		  var key = {};
				    		  key.relevance = response.keywords[i].relevance;
				    		  key.text = response.keywords[i].text;
				    		  var keyword = key.text;
				    			if (keyword.indexOf('Evidence')>-1||
										keyword.indexOf('Indicator')>-1||
										keyword.indexOf('evidence')>-1||
										keyword.indexOf('Guidance')>-1||
										keyword.indexOf('Impact')>-1||
										keyword.indexOf('Ev ID')>-1||
										keyword.indexOf('Ev Guide')>-1||
										keyword.indexOf('END')>-1||
										keyword.indexOf('Indicator ID')>-1) continue;
				    		  
				    		  
				    		  //console.log(key.relevance + ' ' + key.text);
				    		  keylist[keylist.length] = key;
				    	  }
				    	  
				    	  alchemyLanguage['entities'](params, function(err, response) {
				    		  
				    		  for (var i=0;i<response.entities.length;i++){
					    		  var key = {};
					    		  key.relevance = response.entities[i].relevance;
					    		  key.text = response.entities[i].text;
					    		  
					    		  //console.log(key.relevance + ' ' + key.text);
					    		  keylist[keylist.length] = key;
					    	  }
					    	  
				    		  
				    		  
				    		  iblock.keywords = keylist;
					    	  iblock.save(function(err) {
						            if (err) {
						                console.log(err);
						                
						            }else{

						                
						            }
					    	  });
					    	  
				    	  });
				      }
				    }); 
		    });
	    });
		
		setProjectStatus(project.projectName, 'EXTRACTING ENTITIES (ALCHEMY) DONE');
		
		res.end("Keywords extraction in progress. Please check the log for possible errors while invoking Alchemy API.");
	});	
	
}



function generateTrainingDataStandards(project){
	Project.findOne({ projectName: project.projectName }, function(err, existingProject) {
		if (!existingProject){
			console.log("Error, cannot find project: " + project.projectName);
			return;
		}
	
		IBlock.find({doc_type: 'standards',projectName:project.projectName },function (err, iblocks){
			logMessage("Found " + iblocks.length + " iblocks!");
			
			var keys = {};
			for (var i=0;i<iblocks.length;i++){
				var iblock = iblocks[i];
				for (var k=0;k<iblock.keywords.length;k++){
					var keyword = iblock.keywords[k].text;
					var relevance = Math.floor(iblock.keywords[k].relevance * 100);
					if (relevance > 40){
						if (keys[keyword]){
							keys[keyword] += ',"'+iblock.id+'","'+relevance+'"';
						}else{
							keys[keyword] = ',"'+iblock.id+'","'+relevance+'"';
						}
					}
				}
			}
			console.log("generating training data for Standards...");
			logMessage("=======================================================");
			console.log("=======================================================");
			for (k in keys){
				logMessageDirect('"'+k+'"'+keys[k]);
			}
			logMessage("=======================================================");
			console.log("=======================================================");
		});
		
		setProjectStatus(project.projectName, 'GENERATED TRAINIG DATA STANDARDS');
		
	});
}





function generateTrainingDataCompany(project){
	Project.findOne({ projectName: project.projectName }, function(err, existingProject) {
		if (!existingProject){
			console.log("Error, cannot find project: " + project.projectName);
			return;
		}
	
		IBlock.find({doc_type: 'company',projectName:project.projectName },function (err, iblocks){
			logMessage("Found " + iblocks.length + " iblocks!");
			var keys = {};
			for (var i=0;i<iblocks.length;i++){
				var iblock = iblocks[i];
				//logMessage("iblock " + i + " keys " + iblock.keywords.length );
				for (var k=0;k<iblock.keywords.length;k++){
					var keyword = iblock.keywords[k].text;
					//skip common stuff
					
					if (keyword.indexOf('Evidence')>-1||
							keyword.indexOf('Indicator')>-1||
							keyword.indexOf('evidence')>-1||
							keyword.indexOf('Guidance')>-1||
							keyword.indexOf('Impact')>-1||
							keyword.indexOf('Ev ID')>-1||
							keyword.indexOf('Ev Guide')>-1||
							keyword.indexOf('END')>-1||
							keyword.indexOf('Indicator ID')>-1) continue;
					///
					var relevance = Math.floor(iblock.keywords[k].relevance * 100);
					if (relevance > 0){
						if (keys[keyword]){
							keys[keyword] += ',"'+iblock.id+'","'+relevance+'"';
						}else{
							keys[keyword] = ',"'+iblock.id+'","'+relevance+'"';
						}
					}
				}
			}
			console.log("generating training data for Company...");
			logMessage("=======================================================");
			
			for (k in keys){
				logMessageDirect('"'+k+'"'+keys[k]);
			}
			logMessage("=======================================================");
		});

		setProjectStatus(project.projectName, 'GENERATED TRAINIG DATA VERIS');
	});
}


function executeCrossSearch(res, project){

	Settings.findOne({ id: '1' }, function(err, settings) {
		
		var usernameCompany = settings.retrieveRankCompanyUsername;
		var passwordCompany = settings.retrieveRankCompanyPassword;
		var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
		
		var usernameStandards = settings.retrieveRankStandardsUsername;
		var passwordStandards = settings.retrieveRankStandardsPassword;
		var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
			
		var auth = "Basic " + new Buffer(usernameCompany + ":" + passwordCompany).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
		
	    request({
	        url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	var rankers = JSON.parse(body).rankers;
	    	if (rankers.length!=1){
	    		logMessage('ERROR: Comp Rankers found : ' + rankers.length);
	    		res.end('ERROR: Cannot find company fiels ranker (' + rankers.length + ')');
	    		return;
	    	}
	    	var ranker_id = rankers[0].ranker_id;
	    	var rank_url = rankers[0].url;
	    	request({
		        url : rank_url,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	var status = JSON.parse(body).status;
		    	if (status!='Available'){
		    		logMessage('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		res.end('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		return;
		    	}
		    	
		    	Project.findOne({projectName: project.projectName}, function(err, prj){
		    		prj.rankerComp = ranker_id;
			    	prj.save(function(err){if (err) console.log(err)});
		    	});
		    	
		    	logMessage('Ranker ' + ranker_id + ' found and avaialble. Executing search.');
		    	executeCrossSearchWithRankers(res, project, null, ranker_id);
		    });
	    });
	    
	    
	    /* TODO -------------
	    var auth2 = "Basic " + new Buffer(usernameStandards + ":" + passwordStandards).toString("base64");
        var url2 = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
		
	    request({
	        url : url2,
	        headers : {
	            "Authorization" : auth2
	        }
	    },
	    function (error, response, body) {
	    	var rankers = JSON.parse(body).rankers;
	    	if (rankers.length!=1){
	    		logMessage('ERROR: Standards Rankers found : ' + rankers.length);
	    		
	    		return;
	    	}
	    	var ranker_id = rankers[0].ranker_id;
	    	var rank_url = rankers[0].url;
	    	request({
		        url : rank_url,
		        headers : {
		            "Authorization" : auth2
		        }
		    },
		    function (error, response, body) {
		    	var status = JSON.parse(body).status;
		    	if (status!='Available'){
		    		logMessage('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		res.end('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		return;
		    	}
		    	
		    	Project.findOne({projectName: project.projectName}, function(err, prj){
		    		prj.rankerStd = ranker_id;
			    	prj.save(function(err){if (err) console.log(err)});
		    	});
		    	
		    	
		    	//logMessage('Ranker ' + ranker_id + ' found and avaialble. Executing search.');
		    	//executeCrossSearchWithRankers(project, null, ranker_id);
		    });
	    }); 
	    */
	});
}


function executeCrossSearchMatch(res, project){

	Settings.findOne({ id: '1' }, function(err, settings) {
		
		var usernameCompany = settings.retrieveRankCompanyUsername;
		var passwordCompany = settings.retrieveRankCompanyPassword;
		var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
		
		var usernameStandards = settings.retrieveRankStandardsUsername;
		var passwordStandards = settings.retrieveRankStandardsPassword;
		var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
			
		var auth = "Basic " + new Buffer(usernameStandards + ":" + passwordStandards).toString("base64");
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
		
	    request({
	        url : url,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	var rankers = JSON.parse(body).rankers;
	    	if (rankers.length!=1){
	    		logMessage('ERROR: Standards Rankers found : ' + rankers.length);
	    		res.end('ERROR: Cannot find standards fiels ranker (' + rankers.length + ')');
	    		return;
	    	}
	    	var ranker_id = rankers[0].ranker_id;
	    	var rank_url = rankers[0].url;
	    	request({
		        url : rank_url,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	var status = JSON.parse(body).status;
		    	if (status!='Available'){
		    		logMessage('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		res.end('ERROR: Ranker is not in Available status. Current status is: ' + status);
		    		return;
		    	}
		    	
		    	Project.findOne({projectName: project.projectName}, function(err, prj){
		    		prj.rankerStd = ranker_id;
			    	prj.save(function(err){if (err) console.log(err)});
		    	});
		    	
		    	logMessage('Ranker ' + ranker_id + ' found and avaialble. Executing search.');
		    	executeCrossSearchMatchWithRankers(res, project, null, ranker_id);
		    });
	    });
	    
	});
}






function executeCrossSearchWithRankers(res, project, rankerStd, rankerComp){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankCompanyUsername;
		var password = settings.retrieveRankCompanyPassword;
		var solrCluster = settings.retrieveRankCompanySolrCluster;
		var minParagraphSize  = settings.minParagraphSize;
		var keywordsToConsider = settings.keywordsToConsider;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
         
        ReportItem.find({projectName: project.projectName},function(err, iblocks) {}).remove().exec();
        
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrCluster + "/solr/std_solr_config_collection/fcselect?ranker_id="+rankerComp+"&q=";
        var url_post_fix = "&wt=json&fl=id,body,title";
	
        
        IBlock.find({doc_type: 'standards', projectName:project.projectName },function (err, iblocks){
			var results = {};
			for (var i=0;i<iblocks.length;i++){
				var iblock = iblocks[i];
				if (iblock.content[0].text.length  >  minParagraphSize){
								var txt= iblock.content[0].text;
								var doc= iblock.doc;
								var title= iblock.title;
								var iblockid = iblock.id;
								var criteria = ""; 
								var keywords = "";
								var keywords1 = "";
								var score = 0;
								var maxIndex=iblock.keywords.length;
								if (keywordsToConsider < maxIndex){
									maxIndex = keywordsToConsider;
								}
								for (var k=0;k<maxIndex;k++){
									var keyword = iblock.keywords[k].text;
									score += iblock.keywords[k].relevance;
									keywords += keyword +' ';
									keywords1 += keyword +', ';
									keyword = keyword.replace(/ /g, "%20");
									criteria += keyword;
								}
								
								
								var myUrl = url + criteria + url_post_fix;
								
								var req = request.post({
				   	    	        url : myUrl,
				   	    	        headers : {
				   	    	            "Authorization" : auth,
				   	    	            "content-type" : "application/json"
				   	    	        }
				   			    },function (err, resp, body) {
				   				  if (err){
				   					  console.log("Error searching...")
				   				  }else{
				   					 if (body.indexOf('Internal Error') >0 ){
				   						 
				   					 }else{ 
					   					 
				   						try {
				   					        var data = JSON.parse(body);
				   					    } catch(e) {
				   					        //console.log('malformed request', body);
				   					    	logMessage('Cannot parse response.');
				   					    	var ri = new ReportItem({
		   									 	doc_type: 'report_item',
		   									 	projectName: project.projectName,
		   									 	doc: this.doc,
		   									 	id: this.iblockid,
		   									 	title: this.title,
		   									 	txt: this.txt,
		   									 	keywords: this.keywords1,
		   									 	score: this.score
		   							 			});
						   						ri.save(function(err) {
						 						            if (err) {
												                console.log('Error saving report item: ' + err);
												            }
											    });
		   									
		   									return;
				   					    }
				   						 
				   						 if (JSON.parse(body).response){
				   							if (JSON.parse(body).response.numFound == 0){
				   							
				   								var ri = new ReportItem({
					   									 	doc_type: 'report_item',
					   									 	projectName: project.projectName,
					   									 	doc: this.doc,
					   									 	id: this.iblockid,
					   									 	title: this.title,
					   									 	txt: this.txt,
					   									 	keywords: this.keywords1,
					   									 	score: this.score
					   							 			});
						   						 ri.save(function(err) {
						 						            if (err) {
												                console.log('Error saving report item: ' + err);
												            }
											    });
				   							}else{
				   								
				   								
				   								var mydocs = JSON.parse(body).response.docs;
				   								var docsfound = JSON.parse(body).response.numFound;
				   								
				   								var mypayload = {};
				   								mypayload.docx = this.txt;
				   								mypayload.doc1 = mydocs[0]==null?'':mydocs[0].body;
				   								mypayload.doc2 = mydocs[1]==null?'':mydocs[1].body;
				   								mypayload.doc3 = mydocs[2]==null?'':mydocs[2].body;
				   								mypayload.doc4 = mydocs[3]==null?'':mydocs[3].body;
				   								mypayload.doc5 = mydocs[4]==null?'':mydocs[4].body;
				   								
				   								var reqx = request.post({
								   	    	        url : "https://similiarityapp.eu-gb.mybluemix.net/scoreapi",
								   	    	        headers : {
								   	    	            "content-type" : "application/json"
								   	    	        },
								   	    	        json: mypayload
								   			    },function (err, resp, body) {
								   				  if (err){
								   					  console.log("Error searching...")
								   				  }
			   									   		
				   								
						   								if (this.keywords1.length<2) this.keywords1 = '  ';
						   								var ri = new ReportItem({
					   									 	doc_type: 'report_item',
					   									 	projectName: project.projectName,
					   									 	doc: this.doc,
					   									 	id: this.iblockid,
					   									 	title: this.title,
					   									 	txt: this.txt,
					   									 	keywords: this.keywords1.substring(0,keywords1.length-2),
					   									 	score: this.score,
					   									 	matchID: this.mydocs[0].id,
					   									    matchTitle: this.mydocs[0].title,
					   									    matchTxt: this.mydocs[0].body,
					   									    matchedDocScore : Math.floor(body.score1*100),
						   									matchedDocScore2 : Math.floor(body.score2*100),
						   									matchedDocScore3 : Math.floor(body.score3*100),
						   									matchedDocScore4 : Math.floor(body.score4*100),
						   									matchedDocScore5 : Math.floor(body.score5*100)
					   									 });
						   								
						   								 if (docsfound > 1){
						   									 
						   									 ri.matchID2 = this.mydocs[1].id;
						   									 ri.matchTitle2= this.mydocs[1].title;
					   									     ri.matchTxt2= this.mydocs[1].body;
					   									     
						   								 }
						   								 
						   								if (docsfound > 2){
						   									 
						   									 ri.matchID3 = this.mydocs[2].id;
						   									 ri.matchTitle3= this.mydocs[2].title;
					   									     ri.matchTxt3= this.mydocs[2].body;
					   									     
					   									}
						   								 
						   								if (docsfound > 3){
						   									 ri.matchID4 = this.mydocs[3].id;
						   									 ri.matchTitle4 = this.mydocs[3].title;
					   									     ri.matchTxt4 = this.mydocs[3].body;
					   									     
					   									}
						   								
						   								if (docsfound > 4){
						   									 ri.matchID5 = this.mydocs[4].id;
						   									 ri.matchTitle5 = this.mydocs[4].title;
					   									     ri.matchTxt5 = this.mydocs[4].body;
					   									     
					   									}
						   								
								   						ri.save(function(err) {
								 						    if (err) {
														        console.log('Error saving report item: ' + err);
														    }
													    });
								   						
								   			    }.bind({txt:this.txt,iblockid:this.iblockid,doc:this.doc,title:this.title, keywords1:this.keywords1, score:this.score, docsfound:docsfound, mydocs:mydocs}));
				   							}
				   						 }
				   					 }
				   				  }
				   			  }.bind({txt:txt,iblockid:iblockid,doc:doc,title:title, keywords1:keywords1, score:score}));
				}
			}
			
			setProjectStatus(project.projectName, 'CROSS SEARCH EXECUTED');
			res.end('Search in progress. Please allow a few minutes to complete before running the report.');
			
		});
	});
}



function executeCrossSearchMatchWithRankers(res, project, rankerStd, rankerComp){
	Settings.findOne({ id: '1' }, function(err, settings) {
		var username = settings.retrieveRankStandardsUsername;
		var password = settings.retrieveRankStandardsPassword;
		var solrCluster = settings.retrieveRankStandardsSolrCluster;
		var minParagraphSize  = settings.minParagraphSize;
		var keywordsToConsider = settings.keywordsToConsider;
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
         
        MatchedReportItem.find({projectName: project.projectName},function(err, iblocks) {}).remove().exec();
        
        var url = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrCluster + "/solr/std_solr_config_collection/fcselect?ranker_id="+rankerComp+"&q=";
        var url_post_fix = "&wt=json&fl=id,body,title";
	
        
        IBlock.find({doc_type: 'company', projectName:project.projectName },function (err, iblocks){
        	logMessage('iblocks size=' + iblocks.length);
        	var results = {};
			for (var i=0;i<iblocks.length;i++){
				var iblock = iblocks[i];
				if (iblock.content[0].text.length  >  minParagraphSize){
								var txt= iblock.content[0].text;
								var doc= iblock.doc;
								var title= iblock.title;
								var iblockid = iblock.id;
								var iblk = iblock;
								var criteria = ""; 
								var keywords = "";
								var keywords1 = "";
								var score = 0;
								var maxIndex=iblock.keywords.length;
								if (keywordsToConsider < maxIndex){
									maxIndex = keywordsToConsider;
								}
								//maxIndex=4;
								for (var k=0;k<maxIndex;k++){
								
									
									var keyword = iblock.keywords[k].text;
									//console.log(iblock.keywords[k].relevance );
									
									if (keyword.indexOf('Evidence')>-1||
										keyword.indexOf('Indicator')>-1||
										keyword.indexOf('evidence')>-1||
										keyword.indexOf('Guidance')>-1||
										keyword.indexOf('Impact')>-1||
										keyword.indexOf('Ev ID')>-1||
										keyword.indexOf('Ev Guide')>-1||
										keyword.indexOf('END')>-1||
										keyword.indexOf('Indicator ID')>-1||
										criteria.indexOf(keyword)>-1
												) continue; 
									else if (Math.floor(iblock.keywords[k].relevance*100) > 0){
										score += iblock.keywords[k].relevance;
										//console.log(keyword +  ' ' + keyword.indexOf('Indicator'));
										keywords += keyword +' ';
										keywords1 += keyword +', ';
										//keyword = keyword.replace(/ /g, "%20");
										criteria += keyword + ' ';
									}
								}
								criteria = criteria.substring(0,criteria.length-1);
								criteria = criteria.replace(/ /g, "%20");
								
								var myUrl = url + criteria + url_post_fix;
								
								var req = request.post({
				   	    	        url : myUrl,
				   	    	        headers : {
				   	    	            "Authorization" : auth,
				   	    	            "content-type" : "application/json"
				   	    	        }
				   			    },function (err, resp, body) {
				   				  if (err){
				   					  console.log("Error searching...")
				   				  }else{
				   					 if (body.indexOf('Internal Error') >0 ){
				   						 
				   					 }else{ 
				   						try {
				   					        var data = JSON.parse(body);
				   					    } catch(e) {
				   					        //console.log('malformed request', body);
				   					    	logMessage('Cannot parse response.');
			   								var mri = new MatchedReportItem({
		   									 	doc_type: 'report_item',
		   									 	projectName: project.projectName,
		   									 	doc: this.doc,
		   									 	id: this.iblockid,
		   									 	title: this.title,
		   									 	txt: this.txt,
		   									 	score: this.score,
		   									 	keywords: this.keywords1
		   									});
		   									
		   									mri.save(function(err) {
			 						            if (err) {
									                console.log('Error saving report item: ' + err);
									            }
											});
		   									
		   									return;
				   					    }
				   					    
				   					    if (JSON.parse(body).response){
				   							//console.log("ID:" + this.iblockid + " criteria:" + this.criteria + " matched:" + JSON.parse(body).response.numFound);
				   							if (JSON.parse(body).response.numFound > 0 ){
				   								
				   								var parsed_body = JSON.parse(body).response;
				   								var docs = [];
				   								var keyword2=[];
				   								//logMessage("iblk:"+this.iblk.id  + " --------> " + this.iblockid);
				   								//logMessage("-------------------------------");
				   								for (var k=0;k<this.iblk.keywords.length;k++){
				   									var keyword = this.iblk.keywords[k].text;
				   									if (
					   									keyword.indexOf('Evidence')>-1||
														keyword.indexOf('Indicator')>-1||
														keyword.indexOf('evidence')>-1||
														keyword.indexOf('Guidance')>-1||
														keyword.indexOf('Impact')>-1||
														keyword.indexOf('Ev ID')>-1||
														keyword.indexOf('Ev Guide')>-1||
														keyword.indexOf('END')>-1||
														keyword.indexOf('Indicator ID')>-1
				   									){ continue;}
				   									else{
				   										keyword2[keyword2.length]=keyword;
				   									}
				   								}
				   									
				   								for (var l=0;l<keyword2.length;l++){	
					   								logMessage(keyword2[l] + " in " + parsed_body.docs.length);
					   								for (var k = 0;k<parsed_body.docs.length;k++){
					   									if (parsed_body.docs[k].body.indexOf(keyword2[l])>0){
					   										docs[docs.length]=parsed_body.docs[k]; 
					   									}
					   								}
					   							}
				   								
				   								
				   								
				   								
				   								//if (docs.length==0) docs[0]=parsed_body.docs[0];
				   								if (docs.length>0){

						   								var first_doc_body = JSON.parse(body).response.docs[0].body;
						   								var first_doc_title = JSON.parse(body).response.docs[0].title;
						   								var first_doc_id = JSON.parse(body).response.docs[0].id;
						   								
						   								var doc_title2,doc_id2,doc_body2;
						   								var doc_title3,doc_id3,doc_body3;
						   								var doc_title4,doc_id4,doc_body4;
						   								var doc_title5,doc_id5,doc_body5;
						   								
						   								if (docs.length > 1){
						   									doc_title2 = docs[1].title;
						   									doc_id2 = docs[1].id;
						   									doc_body2 = docs[1].body;
						   								}
						   								
						   								if (docs.length > 2){
						   									doc_title3 = docs[2].title;
						   									doc_id3 = docs[2].id;
						   									doc_body3 = docs[2].body;
						   								}
						   								
						   								if (docs.length > 3){
						   									doc_title4 = docs[3].title;
						   									doc_id4 =  docs[3].id;
						   									doc_body4 = docs[3].body;
						   								}
						   								
						   								if (docs.length > 4){
						   									doc_title5 = docs[4].title;
						   									doc_id5 = docs[4].id;
						   									doc_body5 = docs[4].body;
						   								}
						   								
						   								
						   								var mypayload = {};
						   								mypayload.docx = this.txt;
						   								mypayload.doc1 = first_doc_body==null?'':first_doc_body;
						   								mypayload.doc2 = doc_body2==null?'':doc_body2;
						   								mypayload.doc3 = doc_body3==null?'':doc_body3;
						   								mypayload.doc4 = doc_body4==null?'':doc_body4;
						   								mypayload.doc5 = doc_body5==null?'':doc_body5;
						   								
						   								var reqx = request.post({
										   	    	        url : "https://similiarityapp.eu-gb.mybluemix.net/scoreapi",
										   	    	        headers : {
										   	    	            "content-type" : "application/json"
										   	    	        },
										   	    	        json: mypayload
										   			    },function (err, resp, body) {
										   				  if (err){
										   					  console.log("Error searching...")
										   				  }
								   								var mri = new MatchedReportItem({
							   									 	doc_type: 'report_item',
							   									 	projectName: project.projectName,
							   									 	doc: this.doc,
							   									 	id: this.iblockid,
							   									 	title: this.title,
							   									 	txt: this.txt,
							   									 	score: this.score,
							   									 	keywords: this.keywords1,
							   									 	matchedDocTitle : this.first_doc_title,
							   									 	matchedDocTxt : this.first_doc_body,
							   									    matchedDocID : this.first_doc_id,
							   									    matchedDocScore : Math.floor(body.score1*100),
							   									    matchedDocTitle2 : this.doc_title2,
							   									 	matchedDocTxt2 : this.doc_body2,
							   									    matchedDocID2 : this.doc_id2,
							   									    matchedDocScore2 : Math.floor(body.score2*100),
							   									    matchedDocTitle3 : this.doc_title3,
							   									 	matchedDocTxt3 : this.doc_body3,
							   									    matchedDocID3 : this.doc_id3,
							   									    matchedDocScore3 : Math.floor(body.score3*100),
							   									    matchedDocTitle4 : this.doc_title4,
							   									 	matchedDocTxt4 : this.doc_body4,
							   									    matchedDocID4 : this.doc_id4,
							   									    matchedDocScore4 : Math.floor(body.score4*100),
							   									    matchedDocTitle5 : this.doc_title5,
							   									 	matchedDocTxt5 : this.doc_body5,
							   									    matchedDocID5 : this.doc_id5,
							   									    matchedDocScore5 : Math.floor(body.score5*100)
							   									});
								   								
								   								mri.save(function(err) {
								 						            if (err) {
								 						            	logMessage('Error saving report item: ' + err);
														                console.log('Error saving report item: ' + err);
  												                    }
								   								});
										   				  }.bind({txt:this.txt,iblockid:this.iblockid,doc:this.doc,title:this.title, keywords1:this.keywords1, score:this.score, criteria:this.criteria,iblk:this.iblk,
										   					first_doc_title:first_doc_title,first_doc_body:first_doc_body,first_doc_id:first_doc_id,
										   					doc_title2:doc_title2,doc_body2:doc_body2,doc_id2:doc_id2,
										   					doc_title3:doc_title3,doc_body3:doc_body3,doc_id3:doc_id3,
										   					doc_title4:doc_title4,doc_body4:doc_body4,doc_id4:doc_id4,
										   					doc_title5:doc_title5,doc_body5:doc_body5,doc_id5:doc_id5
										   				  }));
								   								
				   								  }else{
				   									  
					   									var mri = new MatchedReportItem({
					   									 	doc_type: 'report_item',
					   									 	projectName: project.projectName,
					   									 	doc: this.doc,
					   									 	id: this.iblockid,
					   									 	title: this.title,
					   									 	txt: this.txt,
					   									 	score: this.score,
					   									 	keywords: this.keywords1
					   									});
					   									
					   									mri.save(function(err) {
						 						            if (err) {
												                console.log('Error saving report item: ' + err);
												            }
														});
				   									  
				   									  
				   								  }
				   								}
				   							}else{
				   								logMessage('Cannot parse response.');
				   								var mri = new MatchedReportItem({
			   									 	doc_type: 'report_item',
			   									 	projectName: project.projectName,
			   									 	doc: this.doc,
			   									 	id: this.iblockid,
			   									 	title: this.title,
			   									 	txt: this.txt,
			   									 	score: this.score,
			   									 	keywords: this.keywords1
			   									});
			   									
			   									mri.save(function(err) {
				 						            if (err) {
										                console.log('Error saving report item: ' + err);
										            }
												});
				   								
				   								
				   							}
				   					 }
				   				  }
				   			  }.bind({txt:txt,iblockid:iblockid,doc:doc,title:title, keywords1:keywords1, score:score, criteria:criteria,iblk:iblk}));
				}
			}
			
			setProjectStatus(project.projectName, 'CROSS SEARCH MATCH EXECUTED');
			res.end('Search in progress. Please allow a few minutes to complete before running the report.');
			
		});
	});
}




function releaseResources(res, project){
	Settings.findOne({ id: '1' }, function(err, settings) {
		
		var usernameCompany = settings.retrieveRankCompanyUsername;
		var passwordCompany = settings.retrieveRankCompanyPassword;
		var solrClusterCompany = settings.retrieveRankCompanySolrCluster;
		
		var usernameStandards = settings.retrieveRankStandardsUsername;
		var passwordStandards = settings.retrieveRankStandardsPassword;
		var solrClusterStandards = settings.retrieveRankStandardsSolrCluster;
		
		
		
		//curl -X DELETE -u "9302d3e0-e608-44a2-beda-e30e9d45e08c":"8Tft0QpmxuwE" "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers/3b140ax14-rank-2756"
			
		var auth = "Basic " + new Buffer(usernameCompany + ":" + passwordCompany).toString("base64");
		var auth2 = "Basic " + new Buffer(usernameStandards + ":" + passwordStandards).toString("base64");
        
		var msg="Deleted rankers and data from SOLR clusters. Report will run from local copy of data.";
	  
		var urla = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
		
	    request({
	        url : urla,
	        headers : {
	            "Authorization" : auth
	        }
	    },
	    function (error, response, body) {
	    	var rankers = JSON.parse(body).rankers;
	    	if (rankers.length!=1){
	    		logMessage('ERROR: Comp Rankers found : ' + rankers.length);
	    		res.end('ERROR: Cannot find company fiels ranker (' + rankers.length + ')');
	    		return;
	    	}
	    	var ranker_id = rankers[0].ranker_id;
		
		
			var urlb = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers/"+ranker_id;
			request.delete({
		        url : urlb,
		        headers : {
		            "Authorization" : auth
		        }
		    },
		    function (error, response, body) {
		    	if (error){
		    		logMessage("Error: cannot delete ranker (comp) : " + ranker_id + " error is: " + error);
		    	}else{
		    		logMessage("Successsfully deleted ranker (comp): " + ranker_id);
		    		msg += "ranker " + ranker_id + " ,";
		    	}
		    });
	    });
	    
	    var urlb = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers";
		
	    request({
	        url : urlb,
	        headers : {
	            "Authorization" : auth2
	        }
	    },
	    function (error, response, body) {
	    	var rankers = JSON.parse(body).rankers;
	    	if (rankers.length!=1){
	    		logMessage('ERROR: Comp Rankers found : ' + rankers.length);
	    		res.end('ERROR: Cannot find company fiels ranker (' + rankers.length + ')');
	    		return;
	    	}
	    	var ranker_id = rankers[0].ranker_id;
		
		
	    
			var url2 = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/rankers/"+ranker_id;
			request.delete({
		        url : url2,
		        headers : {
		            "Authorization" : auth2
		        }
		    },
		    function (error, response, body) {
		    	if (error){
		    		logMessage("Error: cannot delete ranker (std): " + ranker_id + " error is: " + error);
		    	}else{
		    		logMessage("Successsfully deleted ranker (std): " + ranker_id);
		    		msg += "ranker " + ranker_id + " ,";
		    	}
		    });
	    });

		
		
		var url_collections_comp_delete = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterCompany + '/solr/admin/collections?action=DELETE&name=std_solr_config_collection';
	    var url_collections_comp_create = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterCompany + '/solr/admin/collections?action=CREATE&name=std_solr_config_collection&collection.configName=TOOLING_CONFIG_en_2016_08_03';
	    var url_collections_comp_delfile = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterCompany + "/solr/std_solr_config_collection/update?stream.body=<delete><query>*:*</query></delete>&commit=true";
	    
	    	
	    
	    console.log("calling: " + url_collections_comp_delfile);
				
		var req = request({
	    	        url : url_collections_comp_delfile,
	    	        headers : {
	    	            "Authorization" : auth
	    	           
	    	        }
	         },function (err, resp, body) {
	        	 if (err){
	        		 logMessage("ERROR deleting data from company solr cluster: "  + err);
	        	 }
	        	 console.log(body);
	        	 
	        	 /*console.log("calling: " + url_collections_comp_create);
        		
	        	 var req_create = request({
 	    	        url : url_collections_comp_create,
 	    	        headers : {
 	    	            "Authorization" : auth
 	    	           
 	    	        }
	 	         },function (err, resp, body) {
	 	        	 console.log(body);
	 	        	 if (err){
	 	        		 logMessage("ERROR creating empty solr cluster: "  + err);
	 	        	 }else{
	 	        		 logMessage("SUCCESS: empty collection company solr cluster");
	 	        		 
	 	        		 msg += "data from SOLR Cluster " + solrClusterCompany + " ,";
	 	        	 }
	 	         });
	 	         */
        		 msg += "data from SOLR Cluster " + solrClusterCompany + " ,";
        	 
	         });
	    
	    
		var url_collections_std_delete = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterStandards + '/solr/admin/collections?action=DELETE&name=std_solr_config_collection';
	    var url_collections_std_create = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterStandards + '/solr/admin/collections?action=CREATE&name=std_solr_config_collection&collection.configName=TOOLING_CONFIG_en_2016_08_03';
	    var url_collections_std_delfile = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/solr_clusters/" + solrClusterStandards + "/solr/std_solr_config_collection/update?stream.body=<delete><query>*:*</query></delete>&commit=true";
	    
	    console.log("calling: " + url_collections_std_delfile);
	    var req = request({
	    	        url : url_collections_std_delfile,
	    	        headers : {
	    	            "Authorization" : auth2
	    	           
	    	        }
	         },function (err, resp, body) {
	        	 if (err){
	        		 logMessage("ERROR deleting data from standards solr cluster: "  + err);
	        	 }
	        	// console.log("calling: " + url_collections_std_create);
	        	 console.log(body);
        		 /*var req2 = request({
 	    	        url : url_collections_std_create,
 	    	        headers : {
 	    	            "Authorization" : auth2
 	    	           
 	    	        }
		 	         },function (err, resp, body) {
		 	        	console.log(body);
		 	        	 if (err){
		 	        		 logMessage("ERROR deleting data from standards solr cluster: "  + err);
		 	        	 }else{
		 	        		 logMessage("SUCCESS: data deleted from standards solr cluster");
		 	        		 msg += "data from SOLR Cluster " + solrClusterStandards + " !";
		 	        	 }
		 	         });*/
    		 msg += "data from SOLR Cluster " + solrClusterStandards + " !";
	         });
	    setProjectStatus(project.projectName, 'PROJECT CLOSED');
	    //res.end();
	    res.end(msg);
	});
}


function setProjectStatus(projectName, status){
	Project.findOne({ projectName: projectName }, function(err, existingProject) {
        existingProject.status = status;
        existingProject.save(function(err) {
            if (err) {
                logMessage(err);
            }
            logMessage('Project ' +  projectName + ' in status ' + status);
        });
	});
}



app.listen(appEnv.port, appEnv.bind, function() {
  
	//setInterval(stateMachine, 2000); 
	console.log("Node server running on " + appEnv.url);
});