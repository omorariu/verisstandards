var app = angular.module('MEANapp', ['ngRoute', 'ngStorage']);

app.controller('HeaderController', function($scope, $localStorage, $sessionStorage, $location, $http){
    $scope.user = $localStorage;
}); 

app.controller('HomeController', function($scope, $localStorage, $sessionStorage){});

app.controller('DebugController', function($scope, $location, $http, $timeout){
			$scope.reload = function () {
				  $http({
				        method: 'GET',
				        url: '/debug'
				    })
				        .success(function(response){
				            $scope.debug = response;
				            
				            //var scroller = document.getElementById("logarea");
				            //scroller.scrollTop = scroller.scrollHeight;
				        })
				        .error(function(response){
				            console.log(response);
				        }
				    );

			    $timeout(function(){
			      $scope.reload();
			    },2000)
			  };
			  $scope.reload();
			  $scope.clearLog = function() {
			    	 $http({
			             method: 'GET',
			             url: '/debug/clear',
			             })
			             .success(function(response){
			             })
			             .error(function(response){			               
			             });	
				} 
			  	  
});



app.controller('ReportVSCombinedController', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportvs/getjson?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
    })
    .error(function(response){
         console.log(response);
    });
	
    $scope.closeReport = function(){
		  $location.path('/my_projects');
	}
	
    $scope.ignoreRow= function(index) {
	    $scope.reportsItems.splice(index,1);
	}
});


app.controller('ReportSVCombinedController', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportsv/getjson?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
  })
  .error(function(response){
       console.log(response);
  });
	
  $scope.closeReport = function(){
		  $location.path('/my_projects');
	}
	
  $scope.ignoreRow= function(index) {
	    $scope.reportsItems.splice(index,1);
	}
});




app.controller('ReportController', function($scope, $location, $http, $timeout){
	  
	
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportapi/SVExceptions?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
      })
      .error(function(response){
           console.log(response);
       });
	  	  
	  
	  
	  $scope.closeReport = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.ignoreRow= function(index) {
	    	
		  
		  $scope.reportsItems.splice(index,1);
	  }
});



app.controller('ReportM1Controller', function($scope, $location, $http, $timeout){
	  
	
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportapi/SVMatches?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
    })
    .error(function(response){
         console.log(response);
     });
	  	  
	  
	  
	  $scope.closeReport = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.ignoreRow= function(index) {
	    	
		  
		  $scope.reportsItems.splice(index,1);
	  }
});



app.controller('ReportMatchController', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportapi/VSMatches?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
    })
    .error(function(response){
         console.log(response);
     });
	  	  
	  
	  
	  $scope.closeReport = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.ignoreRow= function(index) {
	    	$scope.reportsItems.splice(index,1);
	  }
});

app.controller('ReportMatchX1Controller', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportapi/VSExceptions?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
  })
  .error(function(response){
       console.log(response);
   });
	  	  
	  
	  
	  $scope.closeReport = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.ignoreRow= function(index) {
	    	$scope.reportsItems.splice(index,1);
	  }
});



app.controller('FilterIndicatorsController', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/getIndicators?pName='+pName
	  })
	  .success(function(response){
		  var items = [];
			 for (var obj in response){
				 var kw = '';
				 for (var k=0;k<response[obj].keywords.length;k++){
					 var keyword = response[obj].keywords[k].text;
					 if (response[obj].keywords[k].relevance.toFixed(2)>0){
					 	kw += keyword + '('+response[obj].keywords[k].relevance.toFixed(2)+'), ';
					 }
					 
				 }
				 response[obj].kwords = kw.substring(0,kw.length-2);
				 items[items.length]=response[obj];
			 }
			 
			 $scope.stdItems = items;  
  })
  .error(function(response){
       console.log(response);
   });
	  	  
	  
	  
	  $scope.close = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.removeItem= function(index) {
		  
		    $http({
		        method: 'GET',
		        url: '/removeIndicator?id='+$scope.stdItems[index].id
			})
				  .success(function(response){
					  console.log(response);
			})
			.error(function(response){
			     console.log(response);
			});
  	        $scope.stdItems.splice(index,1);
	  }
});


app.controller('ViewVerisIndicatorsController', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/getVerisIndicators?pName='+pName
	  })
	  .success(function(response){
		 var items = [];
		 //console.log(response);
		 for (var obj in response){
			 //console.log(response[obj]);
			 var kw = '';
			 for (var k=0;k<response[obj].keywords.length;k++){
				 
				 var keyword = response[obj].keywords[k].text;
				 if (keyword.indexOf('Evidence')>-1||
							keyword.indexOf('Indicator')>-1||
							keyword.indexOf('evidence')>-1||
							keyword.indexOf('Guidance')>-1||
							keyword.indexOf('Impact')>-1||
							keyword.indexOf('Ev ID')>-1||
							keyword.indexOf('Ev Guide')>-1||
							keyword.indexOf('END')>-1||
							keyword.indexOf('Indicator ID')>-1) continue;
				 if (response[obj].keywords[k].relevance.toFixed(2)>0){
				 	kw += keyword + '('+response[obj].keywords[k].relevance.toFixed(2)+'), ';
				 }
				 
			 }
			 response[obj].kwords = kw.substring(0,kw.length-2);
			 items[items.length]=response[obj];
		 }
		 
		 
		 
		 $scope.stdItems = items;  
		})
		.error(function(response){
		     console.log(response);
		 });
	  	  
	  
	  
	  $scope.close = function(){
		  $location.path('/my_projects');
	  }
	
});



app.controller('ReportMatchV2Controller', function($scope, $location, $http, $timeout){
	  var urlParams = $location.search();
	  var pName = urlParams.projectName;
	  
	  $scope.myProject = pName;
	  $http({
		        method: 'GET',
		        url: '/reportmatchv2?pName='+pName
	  })
	  .success(function(response){
		  console.log(response);
		 $scope.reportsItems = response;  
  })
  .error(function(response){
       console.log(response);
   });
	  	  
	  
	  
	  $scope.closeReport = function(){
		  $location.path('/my_projects');
	  }
	  
	  $scope.ignoreRow= function(index) {
	    	$scope.reportsItems.splice(index,1);
	  }
});

app.controller('SettingsController', function($scope, $localStorage, $sessionStorage, $http, $location,$timeout){


	$scope.submitForm = function(){
    	$http({
            method: 'POST',
            url: '/settings/save',
            data: {
                    'minParagraphSize': $scope.minParagraphSize,
                    'keywordsToConsider': $scope.keywordsToConsider,
                    'scoreThreshold' : $scope.scoreThreshold,
                    'excludeList' : $scope.excludeList,
                    'retrieveRankStandardsUsername':  $scope.retrieveRankStandardsUsername,
                	'retrieveRankStandardsPassword':  $scope.retrieveRankStandardsPassword,
                	'retrieveRankStandardsSolrCluster': $scope.retrieveRankStandardsSolrCluster,
                	'retrieveRankCompanyUsername':  $scope.retrieveRankCompanyUsername,
                	'retrieveRankCompanyPassword':  $scope.retrieveRankCompanyPassword,
                	'retrieveRankCompanySolrCluster': $scope.retrieveRankCompanySolrCluster,
                	'documentServiceUsername':  $scope.documentServiceUsername,
                	'documentServicePassword':  $scope.documentServicePassword,
                	'alchemyAPIkey': $scope.alchemyAPIkey,
                	'enableOneToOneMatch' : $scope.enableOneToOneMatch
                }
    		})
            .success(function(response){
            	//send standard files
            	alert(response);
            	$timeout(function() {
            		$location.path('/settings');
            	}, 200);
            })
            .error(function(response){
                if(typeof response === 'string'){
                    alert(response);
                }
                
            });

    };
    
    
    $http({
        method: 'GET',
        url: '/settings',
        })
        .success(function(response){
        	$scope.minParagraphSize = response.minParagraphSize;
        	$scope.keywordsToConsider = response.keywordsToConsider;
        	$scope.scoreThreshold = response.scoreThreshold;
        	$scope.excludeList = response.excludeList;
        	$scope.enableOneToOneMatch = response.enableOneToOneMatch;
        	
        	$scope.retrieveRankStandardsUsername = response.retrieveRankStandardsUsername;
        	$scope.retrieveRankStandardsPassword = response.retrieveRankStandardsPassword;
        	$scope.retrieveRankStandardsSolrCluster = response.retrieveRankStandardsSolrCluster;
        	
        	$scope.retrieveRankCompanyUsername = response.retrieveRankCompanyUsername;
        	$scope.retrieveRankCompanyPassword = response.retrieveRankCompanyPassword;
        	$scope.retrieveRankCompanySolrCluster = response.retrieveRankCompanySolrCluster;
        	
        	$scope.documentServiceUsername = response.documentServiceUsername;
        	$scope.documentServicePassword = response.documentServicePassword;
        	$scope.alchemyAPIkey = response.alchemyAPIkey;
        	
        })
        .error(function(response){
            if(typeof response === 'string'){
                alert(response);
            }
        });

});





app.controller('NewProjectController', function($scope, $localStorage, $sessionStorage, $http, $location,$timeout){

	
	$scope.uploadedFileStd = function(element) {
	   	 $scope.$apply(function($scope) {
	   		 $scope.filesStd = element.files;         
	   	 }); 
	} 
	
	$scope.uploadedFileComp = function(element) {
	   	 $scope.$apply(function($scope) {
	   		 $scope.filesComp = element.files;         
	   	 }); 
	} 
	
   	 
    $scope.submitForm = function(){
    	$http({
            method: 'POST',
            url: '/project/new',
            data: {
                    'projectName': $scope.newProject.name,
                    'projectDesc': $scope.newProject.description,
                    'alchemyCustomModel' : $scope.newProject.alchemyCustomModel
                }
            })
            .success(function(response){
            	//send standard files
            	console.log('sending standard files...');
            	var jsonData = {};
            	jsonData.projectName = $scope.newProject.name;
            	var fileList = [];
            	if ($scope.filesStd){
	            	for (var i=0;i<$scope.filesStd.length;i++){
	            		var filex = {};
	            		filex.url = '/project/storestd';
	            		filex.file = $scope.filesStd[i];
	            		fileList[fileList.length] = filex;
	            	}
            	}
            	
            	if ($scope.filesComp){
	            	for (var i=0;i<$scope.filesComp.length;i++){
	            		var filex = {};
	            		filex.url = '/project/storecomp';
	            		filex.file = $scope.filesComp[i];
	            		fileList[fileList.length] = filex;
	            	}
            	
            	}
            	
            	
            	sendFiles($scope.newProject.name, jsonData,fileList,0);
            	
            	
            	console.log('done sending files..');
            	$timeout(function() {
            		$location.path('/my_projects')
            	}, 200);
            })
            .error(function(response){
                if(typeof response === 'string'){
                    alert(response);
                }
                
            }
            );

    };


function sendFiles(pname, jsonData, filesArray, i){
	if (filesArray[i]){
		console.log(JSON.stringify( filesArray[i].file));
		$http({
	        url: filesArray[i].url,
	        method: "POST",
	        headers: { 'Content-Type': undefined },
	        transformRequest: function (data) {
	            var formData = new FormData();
	            formData.append("model", angular.toJson(data.model));
	            formData.append("file",  data.files);
	            return formData;
	        },data: { model: jsonData, files: filesArray[i].file } 
	        
	    }).then(function (response) {
	    	i++;
	    	setTimeout(	sendFiles(pname, jsonData, filesArray, i), 500);
	    })
	}else{
		$http({
	        url: '/project/run',
	        method: "POST",
	        data: {
                'projectName': pname
	        }
	        
	    }).then(function (response) {
	    	;
	    })		
	}
	
}


});



app.controller('MyProjectsController', function($scope, $localStorage, $sessionStorage, $http, $location){
	    $http({
            method: 'GET',
            url: '/projects/list',
            })
            .success(function(response){
                $scope.projectList = response;
            })
            .error(function(response){
                if(typeof response === 'string'){
                    alert(response);
                }
            });

	    
	    $scope.refresh = function() {
	    	      $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
		 } 
	    
	    
	    $scope.deleteProject = function(index) {
	    	console.log('delete.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/delete',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 } 
	    
	    
	    
	    $scope.releaseResources = function(index) {
	    	console.log('releaseResources.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/releaseResources',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    $scope.digestStandardFiles = function(index) {
	    	console.log('digestStandardFiles.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/digestStandardFiles',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    $scope.digestCompanyFiles = function(index) {
	    	console.log('digestCompanyFiles.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/digestCompanyFiles',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                            //alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    $scope.extractEntities = function(index) {
	    	console.log('extractEntities.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/extractEntities',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                          
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    
	    
	    
	    
	    
	    $scope.createSolrClusterStandards = function(index) {
	    	console.log('createSolrClusterStandards.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/createSolrClusterStandards',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    
	    
	    $scope.createSolrClusterCompany = function(index) {
	    	console.log('createSolrClusterCompany.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/createSolrClusterCompany',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    $scope.loadDataStandards = function(index) {
	    	console.log('loadDataStandard.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/loadDataStandard',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	            	 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                	 alert('Loading');
	                 }   // When something else is returned
	                 alert(response);
	             });	
		 }
	    
	    
	    $scope.loadDataCompany = function(index) {
	    	console.log('loadDataCompany.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/loadDataCompany',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	            	 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert('Loading');
	                 }   // When something else is returned
	                 alert(response);
	             });	
		 }
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    $scope.test = function(index) {
	    	console.log('test.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/test',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    $scope.generateTrainingDataStandards = function(index) {
	    	console.log('generateTrainingDataStandards.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/generateTrainingDataStandards',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    $scope.generateTrainingDataCompany = function(index) {
	    	console.log('generateTrainingDataCompany.. '  + $scope.projectList[index].projectName);
	    	
	    	 $http({
	             method: 'POST',
	             url: '/project/generateTrainingDataCompany',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    
	    
	    
	    
	    
	    
	    $scope.trainRRStandards = function(index) {
	    	console.log('trainRRStandards.. '  + $scope.projectList[index].projectName);
	    	
	    	alert('Please run the train.py Python script to generate feature vectors and train the ranker.');
	    	/*
	    	 $http({
	             method: 'POST',
	             url: '/project/trainRRStandards',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });
	             */	
		 }
	    
	    $scope.checkRRS = function(index) {
	    	console.log('CheckRRStandards.. '  + $scope.projectList[index].projectName);
	    	
	    	$http({
	             method: 'POST',
	             url: '/project/checkRRS',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	        })
	        .success(function(response){
	        		 if (response.length>0){
	        			var str = "";
	        			for (var i=0;i<response.length;i++){
	        				console.log(response[i]);
	        			    str += response[i].ranker_id +  "\nCreated: " + response[i].created + "\n Status: " + response[i].status + " Desc:" + response[i].status_description + " \n\n";  	
	        			} 
	        			alert(str);
	        		 }else{
	        			 alert(response);
	        		 }
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	         })
	         .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	         });	
		 }
	    
	    $scope.checkRRV = function(index) {
	    	console.log('CheckRRStandards.. '  + $scope.projectList[index].projectName);
	    	
	    	$http({
	             method: 'POST',
	             url: '/project/checkRRV',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	        })
	        .success(function(response){
		        	if (response.length>0){
	        			var str = "";
	        			for (var i=0;i<response.length;i++){
	        				console.log(response[i]);
	        			    str += response[i].ranker_id + "\nCreated: " + response[i].created +  "\n Status: " + response[i].status + " Desc:" + response[i].status_description + " \n\n";  	
	        			} 
	        			alert(str);
	        		 }else{
	        			 alert(response);
	        		 }
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	         })
	         .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	         });	
		}
	    
	    $scope.deleteRRS = function(index) {
	    	$http({
	             method: 'POST',
	             url: '/project/deleteRRS',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	        })
	        .success(function(response){
	        		 alert(response);
	        		 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	         })
	         .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	         });	
		 }
	    
	    $scope.deleteRRV = function(index) {
	    	$http({
	             method: 'POST',
	             url: '/project/deleteRRV',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	        })
	        .success(function(response){
	        		 alert(response);
	        		 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	         })
	         .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	         });	
		 }
	    
	    
	    
	    
	    $scope.trainRRCompany = function(index) {
	    	console.log('trainRRCompany.. '  + $scope.projectList[index].projectName);

	    	alert('Please run the train.py Python script to generate feature vectors and train the ranker.');
	    	/*
	    	 $http({
	             method: 'POST',
	             url: '/project/trainRRCompany',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	                 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     alert(response);
	                 }   // When something else is returned
	                 
	             });
	             */	
		 }
		
	    
	   
	     $scope.executeCrossMatchSearch = function(index) {
	    	console.log('executeCrossMatchSearch.. '  + $scope.projectList[index].projectName);
	    	 
	    	 $http({
	             method: 'POST',
	             url: '/project/executeCrossMatchSearch',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	            	 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    $scope.executeCrossSearch = function(index) {
	    	console.log('executeCrossSearch.. '  + $scope.projectList[index].projectName);
	    	 
	    	 $http({
	             method: 'POST',
	             url: '/project/executeCrossSearch',
	             data: {
	                     'projectName': $scope.projectList[index].projectName
	                 }
	             })
	             .success(function(response){
	            	 alert(response);
	                 $http({
	                     method: 'GET',
	                     url: '/projects/list',
	                     })
	                     .success(function(response){
	                         $scope.projectList = response;
	                     })
	                     .error(function(response){
	                         if(typeof response === 'string'){
	                             alert(response);
	                         }
	                     });
	             })
	             .error(function(response){
	                 // When a string is returned
	                 if(typeof response === 'string'){
	                     
	                 }   // When something else is returned
	                 
	             });	
		 }
	    
	    $scope.viewReport = function(index) {
	    	console.log('viewReport.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/report').search({projectName: pName});
		}
	    
	    $scope.viewReportMatch = function(index) {
	    	console.log('viewReport.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportm1').search({projectName: pName});
		}
	    
	    $scope.viewSVCombined = function(index) {
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportsvcombined').search({projectName: pName});
		}
	   
	    $scope.viewVSCombined = function(index) {
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportvscombined').search({projectName: pName});
		}
	    
	    
	    $scope.filterIndicators = function(index) {
	    	console.log('filterIndicators.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/filterIndicators').search({projectName: pName});
	    	
	    }
	    
	    
	    $scope.viewVerisIndicators = function(index) {
	    	console.log('viewVerisIndicators.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/viewVerisIndicators').search({projectName: pName});
	    	
	    }


	    $scope.viewMatchReport = function(index) {
	    	console.log('viewMatchReport.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportmatch').search({projectName: pName});
		}
	    
	    $scope.viewMatchReportV2 = function(index) {
	    	console.log('viewMatchReport.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportmatchv2').search({projectName: pName});
		}
	    
	    $scope.viewMatchReportV2Exceptions = function(index){
	    	console.log('view x1 report.. '  + $scope.projectList[index].projectName);
	    	var pName = $scope.projectList[index].projectName;
	    	$location.path('/reportmatchx1').search({projectName: pName});
	    }
	    
	    
	    
});










app.config(function($routeProvider) {
    'use strict';

    $routeProvider.

        //Root
        when('/', {
            templateUrl: 'views/home.html',
            controller: 'HomeController'
        }).

        
        //new project page
        when('/new_project', {
            templateUrl: 'views/new_project.html',
            controller: 'NewProjectController'
        }).

        //my projects page
        when('/my_projects', {
            templateUrl: 'views/my_projects.html',
            controller: 'MyProjectsController'
        }).

        //my projects page
        when('/settings', {
            templateUrl: 'views/settings.html',
            controller: 'SettingsController'
        }).
        //my projects page
        when('/report', {
            templateUrl: 'views/report.html',
            controller: 'ReportController'
        }).
        when('/reportm1', {
            templateUrl: 'views/reportm1.html',
            controller: 'ReportM1Controller'
        }).
        when('/reportmatch', {
            templateUrl: 'views/reportmatch.html',
            controller: 'ReportMatchController'
        }).
        when('/reportmatchv2', {
            templateUrl: 'views/reportmatch2.html',
            controller: 'ReportMatchV2Controller'
        }).
        when('/reportmatchx1', {
            templateUrl: 'views/reportmatchx1.html',
            controller: 'ReportMatchX1Controller'
        }).
        when('/reportsvcombined', {
            templateUrl: 'views/reportsvcombined.html',
            controller: 'ReportSVCombinedController'
        }).
        when('/reportvscombined', {
            templateUrl: 'views/reportvscombined.html',
            controller: 'ReportVSCombinedController'
        }).
        when('/filterIndicators', {
            templateUrl: 'views/filterIndicators.html',
            controller: 'FilterIndicatorsController'
        }).
        when('/viewVerisIndicators', {
            templateUrl: 'views/indicators.html',
            controller: 'ViewVerisIndicatorsController'
        }).
        
      //my projects page
        when('/debug', {
            templateUrl: 'views/debug.html',
            controller: 'DebugController'
        });
        
        

});
