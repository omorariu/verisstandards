from flask import Flask, request, jsonify
from sklearn.feature_extraction.text import TfidfVectorizer

import atexit
import cf_deployment_tracker
import os

# Emit Bluemix deployment event
cf_deployment_tracker.track()

app = Flask(__name__)

db_name = 'mydb'
client = None
db = None

corpus = ['','','','','',''];


# On Bluemix, get the port number from the environment variable PORT
# When running this app on the local machine, default the port to 8080
port = int(os.getenv('PORT', 8080))


@app.route('/scoreapi', methods=['POST'])
def scoreapi():
     myjson = request.get_json();        
     corpus[0]=myjson['docx'];
     corpus[1]=myjson['doc1'];
     corpus[2]=myjson['doc2'];
     corpus[3]=myjson['doc3'];
     corpus[4]=myjson['doc4'];
     corpus[5]=myjson['doc5'];
           
     vectorizer = TfidfVectorizer(min_df=1)
     X = vectorizer.fit_transform(corpus)
     score1 = ((X*X.T).A)[0,1]
     score2 = ((X*X.T).A)[0,2]
     score3 = ((X*X.T).A)[0,3]
     score4 = ((X*X.T).A)[0,4]
     score5 = ((X*X.T).A)[0,5]
     
     return jsonify({'score1': score1,'score2': score2, 'score3': score3, 'score4':score4, 'score5':score5})
       


@atexit.register
def shutdown():
    if client:
        client.disconnect()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, debug=True)
